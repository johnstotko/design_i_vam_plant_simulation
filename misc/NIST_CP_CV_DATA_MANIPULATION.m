% NIST DATA MANIPULATION
%
% This script processes the thermodynamic data given from NIST for CO2,
% Ethylene, N2, O2, and Water. For each thermophysical property, a new
% matlab varialbe will be saved as a 2D array (T,P). 
%
% The left column will be the temperatures [C], the top row will be the
% pressure [Bar]. The top left entry will be zero as a placeholder.
%
% NIST data is in the data folder marked [COMP ID] NIST Data. In the folder
% are tables for isobaric (1,5,10,15 bar) for temperatures between 10 and
% 300 C, when available. 
clc, clear
comps = {'CO2', 'ET', 'N2', 'O2', 'Water', 'C1', 'C2', 'Ar'};
pressure = {' 1', ' 5', ' 10', ' 15'};
textTag = ' bar.txt';
folderTag = ' NIST Data';

for i = 1:length(comps)

    folderName = [comps{i},folderTag];
    data = cell(1,length(pressure));
    Cv = [];
    Cp = [];
    for j = 1:length(pressure)
       
        filepath = ['data/',folderName,'/',comps{i},pressure{j},textTag];
        data = readtable(filepath);
        
        T = table2array(data(:,1));
        
        Cv(:,j+1) = [str2num(pressure{j});table2array(data(:,8))];
        Cp(:,j+1) = [str2num(pressure{j});table2array(data(:,9))];
        
    end
    
    if strcmp(comps{i},'Water')

        Cv(40:41,:) = []; % Odd repeated temperature...
        
        Cp(40:41,:) = [];

        T(40:41,:) = [];
    end
    
    
    Cv(2:end,1) = T;
    save(['data/',comps{i}, ' Cv'],'Cv')
    
    Cp(2:end,1) = T;
    save(['data/',comps{i}, ' Cp'],'Cp')
    


end