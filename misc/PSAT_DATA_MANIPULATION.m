

% This script evaluates the antoine equations for all components and saves
% Psat(T) as a vatiable 
% 
% Data in text files are in the form
%
% T-min     T_max   A   B   C
clc, clear

comp = getCompList();

for i = 1:length(comp)

clear A B C Tmin Tmax T P Ptemp Psat
    
% Get the data
filepath = ['data/Saturation Pressure/',comp{i},'/',comp{i},' Psat.txt'];

try
    data = dlmread(filepath,',');
catch e
    warning(e.message)
    continue
end

Tmin = data(:,1);
Tmax = data(:,2);
A = data(:,3);
B = data(:,4);
C = data(:,5);

Tarray = min(Tmin):5:max(Tmax);
T = repmat(Tarray,length(A));

for j = 1:length(Tarray)
Ptemp = 10.^(A - (B)./(T(:,j) + C)).*...
            and((T(:,j) >= Tmin),(T(:,j) <= Tmax));
P(j) = mean(nonzeros(Ptemp));
end

%T = T - 273; % Change kelvin to Celcius... 
Psat = [Tarray' - 273.15,P'];

% Remove rows that have NaN. Means there was no data available at that T.
k = 1;
while k < size(Psat,1)
    k = k+1;
 if sum(isnan(Psat(k,:))) > 0
     disp('removed')
    Psat(k,:) = []; 
    k = k-1;
 end
end

save(['data/',comp{i},' Psat'], 'Psat')

end







