function [in, out, balance] = checkUnitOverallBalance(streams, inNames, outNames)
% [in, out, balance] = checkUnitOverallBalance(streams, inNames, outNames)
%
% inNames and outNames can be cell arrays or a string if is a single name.
%
% streams is the stream array for the entire process.
%
% All outputs are in kg/hr.

inNames = findStream(streams,inNames);
outNames = findStream(streams, outNames);

comps = getCompList;
in = zeros(1,length(comps));
out = zeros(1,length(comps));

for i = 1:length(comps)
    
in(i) = sum([streams(inNames).(comps{i})].*[streams(inNames).MOLES]);
out(i) = sum([streams(outNames).(comps{i})].*[streams(outNames).MOLES]);

% Convert from mole/s to kg/hour
in(i) = moles2mass(comps{i},in(i))*3600/1000;
out(i) = moles2mass(comps{i},out(i))*3600/1000;

end

in = sum(in);
out = sum(out);
balance = out - in;