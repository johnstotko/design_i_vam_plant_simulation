function [compIn, compOut, compBalance] = checkUnitCompBalance(streams, inNames, outNames, comp)
% [compIn, compOut, compBalance = checkUnitCompBalance(streams, inNames, outNames, comp)
%
% Gives accumulation in kg/hr

inNames = findStream(streams,inNames);
outNames = findStream(streams, outNames);

compIn = sum([streams(inNames).(comp)].*[streams(inNames).MOLES]);
compOut = sum([streams(outNames).(comp)].*[streams(outNames).MOLES]);
compBalance = compOut - compIn;

% Convert from mole/s to kg/hour
compBalance = moles2mass(comp,compBalance)*3600/1000;
compIn = moles2mass(comp,compIn)*3600/1000;
compOut = moles2mass(comp,compOut)*3600/1000;