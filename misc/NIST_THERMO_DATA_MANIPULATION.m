% NIST DATA MANIPULATION
%
% This script processes the thermodynamic data given from NIST for CO2,
% Ethylene, N2, O2, and Water. For each thermophysical property, a new
% matlab varialbe will be saved as a 2D array (T,P). 
%
% The left column will be the temperatures [C], the top row will be the
% pressure [Bar]. The top left entry will be zero as a placeholder.
%
% NIST data is in the data folder marked [COMP ID] NIST Data. In the folder
% are tables for isobaric (1,5,10,15 bar) for temperatures between 10 and
% 300 C, when available. 
clc, clear
comps = {'CO2', 'ET', 'N2', 'O2', 'Water', 'C1', 'C2', 'Ar'};
pressure = {' 1', ' 5', ' 10', ' 15'};
textTag = ' bar.txt';
folderTag = ' NIST Data';

for i = 1:length(comps)

    folderName = [comps{i},folderTag];
    data = cell(1,length(pressure));
    U = [];
    H = [];
    S = [];
    for j = 1:length(pressure)
       
        filepath = ['data/',folderName,'/',comps{i},pressure{j},textTag];
        data = readtable(filepath);
        
        T = table2array(data(:,1));
        U(:,j+1) = [str2num(pressure{j});table2array(data(:,5))];
        H(:,j+1) = [str2num(pressure{j});table2array(data(:,6))];
        S(:,j+1) = [str2num(pressure{j});table2array(data(:,7))];
        
    end
    
    if strcmp(comps{i},'Water')

        U(40,:) = []; % Odd repeated temperature...
        U(41,:) = [];
        
        H(40,:) = []; % Odd repeated temperature...
        H(41,:) = [];
        
        S(40,:) = []; % Odd repeated temperature...
        S(41,:) = [];
        
        T(40,:) = []; % Odd repeated temperature...
        T(41,:) = [];

    end
    
    U(2:end,1) = T;
    
    save(['data/',comps{i}, ' U'],'U')
    
    H(2:end,1) = T;
    save(['data/',comps{i}, ' H'],'H')
    
    S(2:end,1) = T;
    save(['data/',comps{i}, ' S'],'S')
end