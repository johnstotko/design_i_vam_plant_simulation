function isTrue = isMolesNonNegative(streamArray)
%
% Checks of streamArray(i).MOLES are all non-negative
isTrue = true;

for i = 1:length(streamArray) 
    if streamArray(i).MOLES < 0
       isTrue = false; 
    end
end