function isTrue = isStreamNamesUnique(streamArray)
% isvalid = isStreamNamesUnique(streamArray)
%
% Checks if all streams in streamArray have unique names
%
% Outputs a logical binary

isTrue = true;

for i = 1:length(streamArray)
    for j = i+1:length(streamArray)
        
        if strcmp(streamArray(i).Name,streamArray(j).Name)
            isTrue = false;
            error(['Streams ', num2str(i),...
                ' and ',num2str(j),' are both named ',...
                streamArray(i).Name])
            
        end
    end
end
