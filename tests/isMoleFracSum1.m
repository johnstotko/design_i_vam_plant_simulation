function isTrue = isMoleFracSum1(varargin)
% isTrue = isMoleFracValid(streamArray, [tol])
%
% Checks if the mole fractions for each stream in streamArray 
% add up to tol array from 1 if the stream has a flow.
%
% Default tol is 0.0001 if not specified.

if nargin == 1
    
    streamArray = varargin{1};
    tol = 0.0001;
    
elseif nargin == 2
        
    streamArray = varargin{1};
    tol = varargin{2};

else
    
    error('Incorrect number of input arguments')
    
end




isTrue = true;

comps = getCompList();
for i = 1:length(streamArray)
    s = 0;
    for j = 1:length(comps)
       s = s + streamArray(i).(comps{j});  
    end
    
    if abs(1 - s) > tol  && streamArray(i).MOLES ~= 0
       isTrue = false; 
    end
    
end