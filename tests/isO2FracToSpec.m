function isTrue = isO2FracToSpec(streams)

for i = 1:length(streams)
   if streams(i).O2 > 0.08 && ~strcmp(streams(i).Name,'O2 FEED') && ~strcmp(streams(i).Name,'O2 FEED V')
       isTrue = false;
       return
   end
end
isTrue = true;