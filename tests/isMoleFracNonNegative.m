function isTrue = isMoleFracNonNegative(streams)

isTrue = true;
comps = getCompList();
for i = 1:length(streams)
    for j = 1:length(comps)
        
        if streams(i).(comps{j}) < 0
            isTrue = false;
            return
        end  
        
    end
end