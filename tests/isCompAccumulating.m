function [isTrue,ACC] = isCompAccumulating(streams,comp,tol)
% [isTrue,ACC] = isCompAccumulating(streams,comp,tol)

% Stream names that go IN into the process
IN = {'O2 FEED' 'ET FEED' 'AC FEED' 'CWS' 'RXN HOT'};

% Stream names that go OUT of the process
OUT = {'PURGE' 'WASTE WATER' 'BOTS 2' 'PRODUCT' 'RXN FEED'};


% Get stream indexes
IN = findStream(streams, IN);
OUT = findStream(streams, OUT);

compIN = sum([streams(IN).(comp)].*[streams(IN).MOLES]);
compOUT = sum([streams(OUT).(comp)].*[streams(OUT).MOLES]);
ACC = abs(compIN - compOUT);

% Convert from mole/s to kg/hour
ACC = moles2mass(comp,ACC)*3600/1000;

if ACC > tol
    isTrue = false;
else
    isTrue = true;
end