function isTrue = isProductWaterToSpec(streams)

% Get the product stream index
Product = findStream(streams, 'PRODUCT');
SpecComp = 'Water';
Spec = 600e-6;
% Get Total Mass of stream
TotalMass = 0;

comps = getCompList();

for i = 1:length(comps)
   TotalMass = TotalMass + streams(Product).MOLES*...
                            streams(Product).(comps{i})*...
                            molarMass(comps{i});
end

massWater = streams(Product).MOLES * ...
            streams(Product).HAc * ...
            molarMass(SpecComp);
        
if massWater/TotalMass > Spec
    isTrue = false;
else
    isTrue = true;
end