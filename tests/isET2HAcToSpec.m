function isTrue = isET2HAcToSpec(streams)

% Get the index for RXN FEED
RXN_FEED = findStream(streams,'RXN FEED');

ET2HAc = streams(RXN_FEED).ET/streams(RXN_FEED).HAc;

if abs(ET2HAc-3) < 0.0001
    isTrue = true;
else
    isTrue = false;
end