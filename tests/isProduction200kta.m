function [isTrue, diff] = isProduction200kta(streams, tol)
% [isTrue, diff] = isProduction200kta(streams)
% 
% Checks if PRODUCT stream is 200 kta
%
% isTrue is a logical output. True of production stream is to spec, false
% otherwise.
%
% Diff measures the difference between 200 kta and current production

% Figure out what 200 kta (95% stream factor) is in kg/hour
isTrue = true;
spec = 200/0.95; %kt per year
spec = spec/365/24; % kt per hour
spec = spec*10^6; % kg/hour

% Figure out how much is in product stream
% get product stream index
Product = findStream(streams,'PRODUCT');

comps = getCompList;
prod = 0;
for i = 1:length(comps)
    prod = prod + moles2mass(comps{i},...
        streams(Product).MOLES*streams(Product).(comps{i}));
end

prod = prod*3600/1000; % kg/hour

diff = spec - prod;

if abs(diff) > tol
    isTrue = false;
end
    

