function isTrue = isProductHAcToSpec(streams)

% Get the product stream index
Product = findStream(streams, 'PRODUCT');
SpecComp = 'HAc';
Spec = 100e-6;

% Get Total Mass of stream
TotalMass = 0;

comps = getCompList();

for i = 1:length(comps)
   TotalMass = TotalMass + streams(Product).MOLES*...
                            streams(Product).(comps{i})*...
                            molarMass(comps{i});
end

massHAc = streams(Product).MOLES * ...
            streams(Product).HAc * ...
            molarMass(SpecComp);
        
if massHAc/TotalMass > Spec
    isTrue = false;
else
    isTrue = true;
end