function [unit, streams] = runCompressor(unit, streams)
% [unit, streams] = runValve(unit, streams)

% Get stream indexes
Input = findStream(streams, unit.Input);
Output = findStream(streams, unit.Output);

if streams(Input).MOLES ~= 0
T1 = streams(Input).T + 273.15;
P1 = streams(Input).P;
P2 = unit.TargetPressure;

comps = getCompList;
Cp = 0;
for i = 1:length(comps)
    Cp = Cp + streams(Input).(comps{i})*getCp(comps{i},T1,P1);
end

Cv = 0;
for i = 1:length(comps)
    Cv = Cv + streams(Input).(comps{i})*getCv(comps{i},T1,P1);
end

% Set T and P
R = 8.314;

streams(Output).P = unit.TargetPressure;
streams(Output).T = (T1 * ((P2/P1)^(R/Cp))) - 273.15;

% Composition/Mole Flow is identical
streams(Output).MOLES = streams(Input).MOLES;

for i = 1:length(comps)
   streams(Output).(comps{i}) = streams(Input).(comps{i}); 
end

% Duty

unit.Duty = getStreamH(streams(Output)) - getStreamH(streams(Input)); % kJ/s

end

