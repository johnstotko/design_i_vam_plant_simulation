function unit = newPump(varargin)

unit = struct(  'Name', [],...
                'Input', [],... Stream name
                'Output', [],... Stream name
                'Duty', 0,... 
                'TargetPressure', 0);
            
for i = 1:2:nargin
    unit.(varargin{i}) = varargin{i+1};   
end