% Outputs everything we need into one excel file
clc, clear
load('OUTPUT.mat')
%% Process Simulation Output

run('simulationExcelOutput.m')

%% Formatted Streams

run('StreamsToExcel.m')

%% Flammability

run('hsne/FlammabilityLimitsTestEnvironment.m')
input('continue?')

%% Density

run('reports/DensityPrintOut.m')
input('continue?')

%% Viscosity

run('reports/ViscosityPrintOut.m')
input('continue?')


%% K-Value

run('reports/Kvalue.m')
input('continue?')

clc, clear
