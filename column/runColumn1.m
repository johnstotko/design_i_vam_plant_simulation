function [unit, streams] = runColumn1(unit, streams)
% [unit, streams] = runColumn(unit, streams)
%
% Assumes constant molar flow rates, isobaric, and ideal gas law)
%
% VAM = LLK, Non-Distributing
% Water = LK
% Acetic Acid = HK

% Get stream indexes

Input = findStream(streams, unit.Input);
Dist = findStream(streams, unit.Dist);
Bots = findStream(streams, unit.Bots);

P = streams(Input).P; % Assume isobaric throughout

xa = streams(Input).HAc; 
xw = streams(Input).Water;
xv = streams(Input).VAM;

xa = xa/sum([xa,xw,xv]);
xw = xw/sum([xa,xw,xv]);
xv = xv/sum([xa,xw,xv]);  % Normalize the fractions in case these aren't
                        %the only three species.

feed = streams(Input).MOLES; % Feed mole flow rate

if feed ~= 0
%% THINGS WE CAN CHOOSE

ba = 0.99; % Assume bottoms is 99 mole percent of HAc
bw = 0.01; % The balance
bflow = xa * feed; % Assume botom flow rate is about equal to 
                   %the HAc flowing into column.
                   
                   
%% Mass balance

dflow = feed - bflow;
da = ((feed*xa) - (bflow*ba)) / dflow;
dw = ((feed*xw) - (bflow*bw)) / dflow;
dv = (feed * xv) / dflow;

%% Fenske

Aa = 4.68206;
Ba = 1642.54;
Ca = -39.764;

Aw = 5.0768;
Bw = 1659.793;
Cw = -45.854;

Av = 4.34032; % Av = 7.51868; %Av = 5.22841;
Bv = 1299.069; % Bv = 1452.058; %Bv = 1807.332;
Cv = -46.183; % Cv = 240.536; %Cv = 0.726;

p1 = 0;
T = 320;

while abs(P-p1) > 0.01
    
    T = T + 0.005;
    
    pa = 10^(Aa - (Ba / (T + Ca)));
    pw = 10^(Aw - (Bw / (T + Cw)));
    pv = 10^(Av - (Bv / (T + Cv)));
    
    p1 = 1 / ((da/pa) + (dw/pw) + (dv/pv));
    
end

Ttops = T; % Dew Temp

alphadist = pw / pa;

p2 = 0;
T = 320;

while abs(P-p2) > 0.01
    
    T = T + 0.005;
    
    pa = 10^(Aa - (Ba / (T + Ca)));
    pw = 10^(Aw - (Bw / (T + Cw)));
    pv = 10^(Av - (Bv / (T + Cv)));
    
    p2 = (da*pa) + (dw*pw) + (dv*pv);
    
end

Tdist = T;

p3 = 0;
T = 400;
while abs(P-p3) > 0.01
    
    T = T - 0.005;
    
    pa = 10^(Aa - (Ba / (T + Ca)));
    pw = 10^(Aw - (Bw / (T + Cw)));
    
    p3 = (ba*pa) + (bw*pw);
    
end

Tbot = T;
alphabot = pw / pa;

Nmin = log10(((dflow * dw)/(bflow * bw)) * ((bflow * ba)/(dflow * da))) / log10((alphadist * alphabot)^0.5);

%% Underwood

q = 1; % Assume saturated liquid feed

Tfeed = streams(Input).T + 273.15;
pa = 10^(Aa - (Ba / (Tfeed + Ca)));
pw = 10^(Aw - (Bw / (Tfeed + Cw)));

alphapinch = pw / pa;
    
theta = ((alphapinch*xw) + (alphapinch*xa)) / ((xa) + (alphapinch*xw));

Rmin = ((alphapinch*dw) / (alphapinch-theta)) + ((da) / (1 - theta)) - 1;

%% Gilliland

R = 1.30 * Rmin; % Common multiplication factor

X = ((R - Rmin) / (Rmin + 1));

Y = 1 - exp(((1 + (54.4*X)) / (11 + (117.2*X))) * ((X - 1) / (X^0.5)));

N = round(((Nmin - Y) / (1 - Y)), 0);

% Use Kirkbride for Feed Stage

NRtoNS = ((xa/xw)*((bw/da)^2)*(bflow/dflow))^0.206;

NR = round(((NRtoNS / (NRtoNS + 1)) * (N - 1)), 0); % N-1 because not including partial reboiler in this calculation

NS = N - NR; % Feed Stage, if bottom tray is "1"

% N is total stages, including partial reboiler
% R is reflux ratio
% NS is feed tray location, assume the bottom tray being labeled #1

%% Output
% Update the streams and unit

% Convert all temperatures to celcius
Tbot = Tbot - 273.15;
Tdist = Tdist - 273.15;
Ttops = Ttops - 273.15;

% Bottom Stream
streams(Bots).MOLES = bflow;
streams(Bots).HAc = ba;
streams(Bots).Water = bw;
streams(Bots).VAM = 0;
streams(Bots).T = Tbot; 
streams(Bots).P = p3;
% Tops
streams(Dist).MOLES = dflow;
streams(Dist).HAc = da;
streams(Dist).Water = dw;
streams(Dist).VAM = dv;
streams(Dist).T = Tdist;
streams(Dist).P = p1;
% Unit
unit.Stages = N;
unit.TopsTemp = Ttops;
unit.BotsTemp = Tbot;
unit.RefluxRatio = R;
unit.FeedStage = NS;


%% Condenser Duty

% Make a dummy stream for the tops
topsStream = streams(Dist); % Copy it over so everything is the same,   
topsStream.Name = 'Tops';   % except the things explicitly changes.
topsStream.T = Ttops;
topsStream.MOLES = streams(Dist).MOLES*(R+1);

% Make a dummy stream for right after the condenser
condStream = topsStream;
condStream.Name = 'Cond';
condStream.T = Tdist;

% Calcuate Duty
unit.CondensorDuty = (getStreamH(condStream) - getStreamH(topsStream))...
                        *3600*10^-6; % MM kJ/hr


%% Reboiler Duty

% with overall mass balance
unit.ReboilDuty = (-getStreamH(streams(Input))...
                    + getStreamH(streams(Dist))...
                    + getStreamH(streams(Bots)))*3600*10^-6 ... 
                    - unit.CondensorDuty; % MM kJ/hr

end


