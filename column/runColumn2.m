function [unit, streams] = runColumn2(unit, streams)
% [unit, streams] = runColumn(unit, streams)
%
% Assumes constant molar flow rates, isobaric, and ideal gas law)
%
% VAM = LLK, Non-Distributing
% Water = LK
% Acetic Acid = HK

% Get stream indexes

Input = findStream(streams, unit.Input);
Dist = findStream(streams, unit.Dist);
Bots = findStream(streams, unit.Bots);

P = streams(Input).P; % Assume isobaric throughout

xa = streams(Input).HAc; 
xw = streams(Input).Water;
xv = streams(Input).VAM;

xa = xa/sum([xa,xw,xv]);
xw = xw/sum([xa,xw,xv]);
xv = xv/sum([xa,xw,xv]);  % Normalize the fractions in case these aren't
                        %the only three species.

feed = streams(Input).MOLES; % Feed mole flow rate

if feed ~= 0
%% THINGS WE CAN CHOOSE

da = 0.0001429; % Assume bottoms is 99 mole percent of HAc
dv = 0.997;
dw = 1 - dv - da; % The balance
dflow = 0.999*((streams(Input).MOLES)*streams(Input).VAM) / dv; % Assume botom flow rate is about equal to 
                   %the VAM flowing into column.
                                    
%% MASS BALANCE

bflow = feed - dflow;
ba = ((feed*xa) - (dflow*da)) / bflow;
bw = ((feed*xw) - (dflow*dw)) / bflow;
bv = ((feed*xv) - (dv*dflow)) / bflow;


alphaFEED = Psat('VAM',streams(Input).T)/Psat('Water',streams(Input).T);

%% FENSKE

Aa = 4.68206;
Ba = 1642.54;
Ca = -39.764;

Aw = 5.0768;
Bw = 1659.793;
Cw = -45.854;

Av = 4.34032; % Av = 7.51868; %Av = 5.22841;
Bv = 1299.069; % Bv = 1452.058; %Bv = 1807.332;
Cv = -46.183; % Cv = 240.536; %Cv = 0.726;

p1 = 0;
T = 320;

while abs(P-p1) > 0.01 
    T = T + 0.005;
    
    pa = 10^(Aa - (Ba / (T + Ca)));
    pw = 10^(Aw - (Bw / (T + Cw)));
    pv = 10^(Av - (Bv / (T + Cv)));
    
    p1 = 1 / ((da/pa) + (dw/pw) + (dv/pv));  
end

Ttops = T; % Dew Temp

alphadist = pv / pw;

p2 = 0;
T = 320;

while abs(P-p2) > 0.01
    
    T = T + 0.005;
    
    pa = 10^(Aa - (Ba / (T + Ca)));
    pw = 10^(Aw - (Bw / (T + Cw)));
    pv = 10^(Av - (Bv / (T + Cv)));
    
    p2 = (da*pa) + (dw*pw) + (dv*pv);
    
end

Tdist = T-0.2; % A bit extra cooling. It's pure VAM so the bubble 
               % and dew temps are really close. 

p3 = 0;
T = 400;

while abs(P-p3) > 0.01
    
    T = T - 0.005;
    
    pa = 10^(Aa - (Ba / (T + Ca)));
    pw = 10^(Aw - (Bw / (T + Cw)));
    pv = 10^(Av - (Bv / (T + Cv)));
    
    p3 = (ba*pa) + (bw*pw) + (bv*pv);
    
end

Tbot = T;
alphabot = pv / pw;

Nmin = log10(((dflow * dw)/(bflow * bw)) * ((bflow * ba)/(dflow * da))) / log10((alphadist * alphabot)^0.5);

%% UNDERWOOD

q = 1; % Assume saturated liquid feed

Lmin  = feed * ((((dflow*dv)/(feed*xv)) - (alphaFEED*((dflow*dw)/(feed*xw)))) / (alphaFEED - 1));

Rmin = Lmin / dflow;

%% Gilliland

R = 1.30 * Rmin; % Common multiplication factor

X = ((R - Rmin) / (Rmin + 1));

Y = 1 - exp(((1 + (54.4*X)) / (11 + (117.2*X))) * ((X - 1) / (X^0.5)));

N = round(((Nmin - Y) / (1 - Y)), 0);

% Use Kirkbride for Feed Stage

NRtoNS = ((xa/xw)*((bw/da)^2)*(bflow/dflow))^0.206;

NR = round(((NRtoNS / (NRtoNS + 1)) * (N - 1)), 0); % N-1 because not including partial reboiler in this calculation

NS = N - NR; % Feed Stage, if bottom tray is "1"

% N is total stages, including partial reboiler
% R is reflux ratio
% NS is feed tray location, assume the bottom tray being labeled #1

%% OUTPUT
% Update the streams and unit

% Convert all temps to celcius
Tbot = Tbot - 273.15;
Tdist = Tdist - 273.15;
Ttops = Ttops - 273.15;

% Bottom Stream
streams(Bots).MOLES = bflow;
streams(Bots).HAc = ba;
streams(Bots).Water = bw;
streams(Bots).VAM = bv;
streams(Bots).T = Tbot;
streams(Bots).P = p3;
% Tops
streams(Dist).MOLES = dflow;
streams(Dist).HAc = da;
streams(Dist).Water = dw;
streams(Dist).VAM = dv;
streams(Dist).T = Tdist;
streams(Dist).P = p1;
% Unit
unit.Stages = N;
unit.TopsTemp = Ttops;
unit.BotsTemp = Tbot;
unit.RefluxRatio = R;
unit.FeedStage = NS;

%% Condenser Duty

% Make a dummy stream for the tops
topsStream = streams(Dist); % Copy it over so everything is the same,   
topsStream.Name = 'Tops';   % except the things explicitly changes.
topsStream.T = Ttops;
topsStream.MOLES = streams(Dist).MOLES*(R+1);

% Make a dummy stream for right after the condenser
condStream = topsStream;
condStream.Name = 'Cond';
condStream.T = Tdist;

% Calcuate Duty
unit.CondensorDuty = (getStreamH(condStream) - getStreamH(topsStream))...
                        *3600*10^-6; % MM kJ/hr


%% Reboiler Duty

% with overall energy balance
unit.ReboilDuty = (-getStreamH(streams(Input))...
                    + getStreamH(streams(Dist))...
                    + getStreamH(streams(Bots)))*3600*10^-6 ... 
                    - unit.CondensorDuty; % MM kJ/hr

end


