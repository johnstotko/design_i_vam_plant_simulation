function [unit, streams] = runHeatX(unit,streams)

% GET Stream Indexes

HOT_Input = findStream(streams, unit.HOT_Input);
HOT_Output = findStream(streams, unit.HOT_Output);
COLD_Input = findStream(streams, unit.COLD_Input);
COLD_Output = findStream(streams, unit.COLD_Output);

% Initiate streams to do nothing

streams(HOT_Output) = streams(HOT_Input);
streams(HOT_Output).Name = unit.HOT_Output;

streams(COLD_Output) = streams(COLD_Input);
streams(COLD_Output).Name = unit.COLD_Output;


if streams(HOT_Input).MOLES ~= 0 && streams(COLD_Input).MOLES ~= 0

    streams(HOT_Output).T = streams(COLD_Input).T + unit.ColdApproach;
    
    % Q = mCpdT
    Q = streams(HOT_Input).MOLES * ...
            (H('Water',streams(HOT_Output).T,streams(HOT_Output).P) - ...
             H('Water',streams(HOT_Input).T ,streams(HOT_Input).P))*1000;% J/s
         
    unit.Duty = -Q*3600*10^-9; % MM kJ/hr
        
    dT = -Q/(streams(COLD_Input).MOLES * getStreamCp(streams(COLD_Input)));

    streams(COLD_Output).T = dT + streams(COLD_Input).T;

    if(streams(COLD_Input).T > streams(HOT_Output).T) || ...
        (streams(COLD_Output).T > streams(HOT_Input).T)
        warning('Temperatures crossed in the HEAT X')
    end
               
end




