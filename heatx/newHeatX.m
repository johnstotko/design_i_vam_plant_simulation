function unit = newHeatX(varargin)

unit = struct(  'Name', [],...
                'HOT_Input', [],... Stream name
                'HOT_Output', [],... Stream name
                'COLD_Input', [],... Stream name
                'COLD_Output', [],... Stream name
                'OHTC', 1,... Overall Heat Transfer Coeffecient
                'ColdApproach', 0, ... Cold side dT
                'Duty', 0,... 
                'Pressure_Drop', 0);
            
for i = 1:2:nargin
    unit.(varargin{i}) = varargin{i+1};   
end