%% Density
clc, clear
load('OUTPUT.mat')
    fprintf('%s \t %s \t %s \n', ...
                'Stream',...
                'Density', 'Phase')
phase = {'vapor',...
            'liquid',...
            'vapor',...
            'vapor',...
            'vapor',...
            'liquid',...    % AC FEED V
            'mix',...       % FEED MIX
            'vapor',...     % RXN FEED
            'vapor',...     % RXN HOT
            'liquid',...    % CWS 2
            'vapor',...     % CWS 2 HOT
            'vapor',...     % CWS 2 COOL
            'vapor',...     % CWS 2 COLD
            'vapor',...     % RXN HOT V
            'mix',...       % RXN COND
            'vapor',...     % RXN VAP
            'liquid',...    % RXN LIQ
            'liquid',...    % RXN LIQ P
            'liquid',...    % COL 1 FEED
            'liquid',...    % TOPS
            'liquid',...    % BOTS 1
            'liquid',...    % HAC RECYCLE
            'liquid',...    % ABS LIQ
            'liquid',...    % ABS LIQ P
            'liquid',...    % WASTE WATER
            'liquid',...    % RAFFINATE
            'liquid',...    % RAFFINATE P
            'liquid',...    % EXTRACT
            'liquid',...    % EXTRACT 2
            'liquid',...    % CWS
            'liquid',...    % CWS-T
            'liquid',...    % ABS WASH
            'vapor',...     % C-VAP
            'vapor',...     % C-VAP-P
            'vapor',...     % PURGE
            'vapor',...     % ET REC
            'liquid',...    % EXTRACT MIX
            'liquid',...    % COL 2 FEED
            'liquid',...    % BOTS 2
            'liquid'};      % PRODUCT


for i = 1:length(streams)
    fprintf('%s \t %f \t %s \n', ...
                streams(i).Name,...
                getStreamDensity(streams(i), phase{i}),...
                phase{i})
end