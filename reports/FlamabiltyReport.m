clc, clear
import mlreportgen.report.*
import mlreportgen.dom.*

prep;
load('OUTPUT.mat');

R = Report('Flammability Diagrams', 'pdf');
titlePage = TitlePage('Title','Flammability Report',...
                        'Author', 'John Stotko',...
                        'Subtitle', 'Messing around with MATLAB Report Generator');
add(R,titlePage);

try
    for i = 1:length(streams)

        if getFuelFrac(streams(i)) ~= 0

        add(R,PageBreak());

        text = Text(streams(i).Name);
        add(R,text);

        fig = plotFlamability(streams(i),false,false);
        filename = ['figure' num2str(i) '.png'];
        saveas(fig,filename);
        img = Image(filename);
        img.Style = {ScaleToFit};

        add(R,img);

        end
    end
catch e
warning(e.Message)
end
close(R)
close all