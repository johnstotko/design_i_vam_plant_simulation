%% Production Selectivity Report
%
% This report tests the sensativity of PRODUCT flow rate to REACTOR inlet
% flow rate. 
clc, clear

%% Test Variables
minTargetIn = 616;
maxTargetIn = 619;
gapTargetIn = 1;

%% TOLERANCE SETTINGS
ConvergenceTol = 0.1; % [kg/hr]
stepGap = 60;

%% BEGIN TEST

prep

targetIn = minTargetIn:gapTargetIn:maxTargetIn;
production = zeros(size(targetIn));

   %run('simulation/feedStreams.m')         % NOTE: By loading these here, 
   %run('simulation/proccessStreams.m')     % each run will use the stream 
   %run('simulation/proccessUnits.m')       % state from the last run. This 
                                            % saves on run time, but may
                                            % afftect results.. needs
                                            % testing.
    
for target = 1:length(targetIn)
    fprintf('Target Reactor Inlet: %f \n\t%i tests remaining.\n',...
                        targetIn(target),...
                        length(targetIn)-target)
    clearvars -except production ConvergenceTol stepGap targetIn target               
                    
    run('simulation/feedStreams.m')        
    run('simulation/proccessStreams.m')     
    run('simulation/proccessUnits.m')  

    % This is the test variable
    reactors(1).TargetFeed = targetIn(target);

    converged = false;
    n = 0;
    while  ~converged

    run('simulation/runSequence.m')
    n = n+stepGap;

    [converged, accumulation] = isConverged(streams,ConvergenceTol);
    fprintf('Itteration %i \n \t Accumulation: %7.3f [kg/hour] \n', n, accumulation);

    end

    production(target) = streams(findStream(streams,'PRODUCT')).MOLES;

end

%% OUTPUT

plot(targetIn,production)
xlabel('Feed to Reactor')
ylabel('Production')

