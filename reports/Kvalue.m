clc, clear
load('OUTPUT.mat')

fprintf('Unit \t K-Value \t \n')
% Column 1
stream = streams(findStream(streams,'COL 1 FEED'));

Water = Psat('Water',stream.T)/stream.P;
HAc = Psat('HAc',stream.T)/stream.P;
VAM = Psat('VAM',stream.T)/stream.P;

K = geomean([Water, HAc, VAM]);


fprintf('%s \t %2.5f \t \n', 'COL 1', K);

% Column 2
stream = streams(findStream(streams,'COL 2 FEED'));

Water = Psat('Water',stream.T)/stream.P;
HAc = Psat('HAc',stream.T)/stream.P;
VAM = Psat('VAM',stream.T)/stream.P;

K = geomean([Water, HAc, VAM]);

fprintf('%s \t %2.5f \t \n', 'COL 2', K);


% Absorber

stream = streams(findStream(streams,'ABS LIQ'));

Water = Psat('Water',stream.T)/stream.P;
HAc = Psat('HAc',stream.T)/stream.P;
VAM = Psat('VAM',stream.T)/stream.P;

K = geomean([Water, HAc, VAM]);

fprintf('%s \t %2.5f \t \n', 'ABS 1', K);







