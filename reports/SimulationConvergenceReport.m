%% Simulation Convergence Test
%
% This script evaluates an overall mass balance for the process every n
% itterations and produces a accumulation [kg/hr] vs. itterations plot. 

clc, clear
prep

%% TEST VARIABLES
stepGap = 1;
nMAX = 500;


%% INITIALIZATION
n = stepGap:stepGap:nMAX;
ACC = zeros(1,length(n));

run('simulation/feedStreams.m')
run('simulation/proccessStreams.m')
run('simulation/proccessUnits.m')

%% BEGIN TEST
for s = 1:length(n)
fprintf('Starting %i itteration \n', n(s))
% Do next set of itterations
run('simulation/runSequence.m')

% Tabulate Accumulation
[~,~,ACC(s)] = checkUnitOverallBalance(streams,process.Inputs,process.Outputs);

end

%% OUTPUT

loglog(n,abs(ACC))

xlabel('Number of Itterations')
ylabel('Accumulation [kg/hr]')
title('Accumulation vs. Number of Itterations')







