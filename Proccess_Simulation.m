%% Process_Simulation
clc, clear

%% TOLLERANCE SETTINGS

ConvergenceTol = 0.1; % [kg/hr]
ProductionSpecTol = 15; % [kg/hr]

%% CONTROL SETTINGS

stepGap = 30;

%% Initialize Process

prep
run('simulation/feedStreams.m')
run('simulation/proccessStreams.m')
run('simulation/proccessUnits.m')

%% PRE-TESTS
disp('-------------------------------------------------------------------')
disp('PRE TESTS')
disp('-------------------------------------------------------------------')
disp(' ')
run('simulation/preTests.m')
disp(' ')

%% CALCULATE

disp('-------------------------------------------------------------------')
disp('SIMULATION')
disp('-------------------------------------------------------------------')
disp(' ')

n = 0;
converged = false;
productionMet = false;

while  ~converged
run('simulation/runSequence.m')
n = n+stepGap;

[converged, accumulation] = isConverged(streams,ConvergenceTol);
[productionMet, margin ] = isProduction200kta(streams,ProductionSpecTol);
fprintf('Itteration %i \n \t Accumulation: %7.3f [kg/hour] \n \t Prod. Margin: %7.3f [kg/hour]\n',...
        n, accumulation, margin);
end

%% POST-TESTS
disp('-------------------------------------------------------------------')
disp('POST TESTS')
disp('-------------------------------------------------------------------')
disp(' ')
run('simulation/postTests.m')
disp(' ')
disp(' ')
%% OUTPUT
disp('-------------------------------------------------------------------')
disp('OUTPUT')
disp('-------------------------------------------------------------------')
% TO COMMAND WINDOW?
%run('simulation/simulationCommandOutput')

% TO EXCEL SHEET?
%run('simulation/simulationExcelOutput')
%disp(struct2table(streams))

disp('Finished!')

s = input('Save?  (Y/[N])', 's');

if strcmp(s,'Y')
    save('OUTPUT.mat')
end



