clear
load('OUTPUT.mat')

test = 8;

switch test
    case 1 % COOLER 1
        OUT = findStream(streams,'RXN COND');
        IN = findStream(streams,'RXN HOT V');
    case 2 % HEATER 1
        OUT = findStream(streams,'RXN FEED');
        IN = findStream(streams,'FEED MIX');
    case 3 % HEAT X COLD
        OUT = findStream(streams,'COL 1 FEED');
        IN = findStream(streams,'RXN LIQ');
    case 4 % HEAT X HOT
        OUT = findStream(streams,'CWS 2 COOL');
        IN = findStream(streams,'CWS 2 HOT');
    case 5 % REACTOR WATER
        OUT = findStream(streams,'CWS 2 HOT');
        IN = findStream(streams,'CWS 2');
    case 6 % COMP
        OUT = findStream(streams,'ET REC');
        IN = findStream(streams,'C-VAP-P');
    case 7 % HAC PUMP
        OUT = findStream(streams,'BOTS 1');
        IN = findStream(streams,'HAC RECYCLE');
    case 8 % RAFF PUMP
        OUT = findStream(streams,'RAFFINATE P');
        IN = findStream(streams,'RAFFINATE');
end


dut = (getStreamH(streams(OUT)) - getStreamH(streams(IN))); % kJ/s

fprintf('Duty: %5.2f kJ/s \n',dut)