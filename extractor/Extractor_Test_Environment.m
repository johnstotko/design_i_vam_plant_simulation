%% EXTRACTOR TEST ENVIRONMENT
%
% This script tests the decanter (extractor) unit seperate from the full
% process. Only use this script to debug the decanter, not to perform
% process calculations.

clc, clear

%% STREAMS
% Define inlet stream

streams(1) = newStream('Name', 'COL 1 TOPS',...
                    'MOLES',100,...
                    'HAc' , .04,...
                    'VAM', 0.48,...
                    'Water', 0.48);
                
% initiate other process streams

streams(2) = newStream('Name', 'Organic 1');
streams(3) = newStream('Name', 'Aqueous 1');



%% UNITS

decanters(1) = newDecanter('Name', 'LL-EXT 1',...
                           'Input', 'COL 1 TOPS',...
                           'OrganicOutput', 'Organic 1',...
                           'AqueousOutput', 'Aqueous 1');
                

                       
%% RUN

[decanters(1),  streams] = runDecanter(decanters(1), streams);

%% POST-TESTS


%% Output 
disp(struct2table(streams))