function [xBOUNDARY,yBOUNDARY,tieLINES] = JulianSmithTernaryData()

BoundaryDataFile = 'TABLE1-A-JulianCSmith.txt';
TieLineDataFile = 'TABLE1-B-JulianCSmith.txt';

%% Boundary                                                    
DATA_BOUNDARY = dlmread(BoundaryDataFile);

DATA_BOUNDARY = DATA_BOUNDARY/100;
[xBOUNDARY,yBOUNDARY]=rect2tern(DATA_BOUNDARY(:,1), DATA_BOUNDARY(:,2));

%% Tie Lines

DATA_GROSS = dlmread(TieLineDataFile);

DATA_GROSS = DATA_GROSS/100;

% xGROSS, yGROSS is the feed compositions for the tie lines
[xFEED,yFEED]=rect2tern(DATA_GROSS(:,1),DATA_GROSS(:,2));

% Rectify data with other boundary data

% TIE_W is the right side of the tie line.
% It'll be treated as the Raffinate.

% TIE_W = [WATER, VAM, HAc]
TIE_W = zeros(size(DATA_GROSS(:,3:5)));
TIE_W(:,1) = DATA_GROSS(:,5);
TIE_W(:,2) = NaN;
TIE_W(:,3) = DATA_GROSS(:,4);


% Fill in holes
[~,maxINDEX] = max(DATA_BOUNDARY(:,3));
maxINDEX = maxINDEX-1;
for i = 1:6
z = find(~isnan(TIE_W(i,:)));
water = interp1(DATA_BOUNDARY(maxINDEX:end,z),...
                DATA_BOUNDARY(maxINDEX:end,2),....
                TIE_W(i,z));
NEWTIE = [0, water, 0];
NEWTIE(z) = TIE_W(i,z);
missing = (z+2)*(z==1) + (z-2)*(z==3);
NEWTIE(missing) = 1 - sum(NEWTIE);

TIE_W(i,:) = NEWTIE;

end

[xRAFF,yRAFF]=rect2tern(TIE_W(:,1),TIE_W(:,2));

% Get the equation for each tie line
% y = mx + b, or y = [m, b]
m = (yRAFF - yFEED)./(xRAFF - xFEED);
tieLINES = [m, yFEED - m.*xFEED];

% Add in the bottom line
tieLINES = [0,0;tieLINES];