function [unit, streams] = runDecanter(unit, streams)

% Get stream indexes
Input = findStream(streams,unit.Input);
OrganicOutput = findStream(streams,unit.OrganicOutput);
AqueousOutput = findStream(streams,unit.AqueousOutput);

% Get input stream, 3 component mole flow
IN = streams(Input).MOLES *[streams(Input).Water;
                            streams(Input).VAM  ;
                            streams(Input).HAc ];

% Okay, so IN is an array containing molar flow rate of these three
% components... get extract is expecting mass flow rates. So let's convert.

IN = IN.*[18.01528;
            86.09;
            60.05];
                        
% Run the extraction
[EXTRACT, RAFFINATE] = extract(IN);

% Again, Extract() runs mass, so EXTRACT and RAFFINATE are mass flow rates.
% Let's convert them to molar flow rates

EXTRACT = EXTRACT.*[1/18.01528;
                    1/86.09;
                    1/60.05];
                
RAFFINATE = RAFFINATE.*[1/18.01528;
                        1/86.09;
                        1/60.05];
                    
% It is easier to plug in total mole flow of each component right
% now. This will get recalculated to be mole frac at the end.

streams(OrganicOutput).Water = EXTRACT(1);
streams(OrganicOutput).VAM = EXTRACT(2);
streams(OrganicOutput).HAc = EXTRACT(3);

streams(AqueousOutput).Water = RAFFINATE(1);
streams(AqueousOutput).VAM = RAFFINATE(2);
streams(AqueousOutput).HAc = RAFFINATE(3);

% Now to decide where the other components go...
%
% For now, let's check if there are any. If there are not, let's carry on
% as normal. If there are tract elements, then we will just put them in the
% AqueousOutput and proc a warning.


comps = getCompList();

for i = 1:length(comps)

    if  ~strcmp(comps{i},'Water') &&...
        ~strcmp(comps{i},'VAM') &&...
        ~strcmp(comps{i},'HAc')
    
    streams(AqueousOutput).(comps{i}) = ...
            streams(Input).MOLES*streams(Input).(comps{i}); 
        
    if streams(AqueousOutput).(comps{i}) ~= 0
        warning('Behavior of ',comps{i},' not specified', ...
           ' in extractor. Defaulted to aqueoues phase') 
    end
        
    end
end

% Now to convert back to mole frac

% Get the total mole flow
streams(OrganicOutput).MOLES = 0;
streams(AqueousOutput).MOLES = 0;

for i = 1:length(comps)
   
    streams(OrganicOutput).MOLES = ...
    streams(OrganicOutput).MOLES + streams(OrganicOutput).(comps{i});
   
    streams(AqueousOutput).MOLES = ...
    streams(AqueousOutput).MOLES + streams(AqueousOutput).(comps{i});
    
end



% Convert to mole frac
if      (streams(AqueousOutput).MOLES ~= 0) && ...
        (streams(OrganicOutput).MOLES ~= 0)
    for i = 1:length(comps)
    
    streams(OrganicOutput).(comps{i}) = ...
        streams(OrganicOutput).(comps{i})/streams(OrganicOutput).MOLES;
    streams(AqueousOutput).(comps{i}) = ...
        streams(AqueousOutput).(comps{i})/streams(AqueousOutput).MOLES;
    end
end

% Let's not forget about temperature and pressure
streams(AqueousOutput).P = streams(Input).P - unit.Pressure_Drop;
streams(OrganicOutput).P = streams(Input).P - unit.Pressure_Drop;

streams(AqueousOutput).T = streams(Input).T;
streams(OrganicOutput).T = streams(Input).T;



