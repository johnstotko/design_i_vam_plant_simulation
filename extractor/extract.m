function [RAFFINATE, EXTRACT] = extract(IN)
% IN, LIGHT, HEAVY are 3 component streams [H2O, VAM, HAC] in mass flow
% rate

% TODO: Change the sum to mix
FEED = sum(IN);

if FEED == 0
    EXTRACT = [0;0;0];
    RAFFINATE = [0;0;0];
    return
end

IN_frac = IN/FEED;

[xBOUNDARY,yBOUNDARY,tieLINES] = JulianSmithTernaryData();

[x,y] = tieLine(xBOUNDARY,yBOUNDARY,tieLINES,IN_frac(1),IN_frac(2));

EXTRACT = [x(1);y(1);1-x(1)-y(1)];
RAFFINATE = [x(2);y(2);1-x(2)-y(2)];

if RAFFINATE == EXTRACT
    Flow = [FEED/2; FEED/2];
    warning('One Phase in Decanter')
    
else
    
    C = dist(RAFFINATE(1:2),IN_frac(1:2))/dist(EXTRACT(1:2),IN_frac(1:2));
    
    Flow(1) = (FEED*C)/(1+C);
    Flow(2) = (FEED)/(1+C);
    
end


EXTRACT = Flow(1)*EXTRACT;
RAFFINATE = Flow(2)*RAFFINATE;