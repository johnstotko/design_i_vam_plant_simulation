function unit = newDecanter(varargin)

unit = struct(  'Name', [],...
                'Input', [],... Stream name
                'OrganicOutput', [],... Stream name
                'AqueousOutput', [],... Stream name
                'Pressure_Drop', 0);
            
for i = 1:2:nargin
    unit.(varargin{i}) = varargin{i+1};   
end

