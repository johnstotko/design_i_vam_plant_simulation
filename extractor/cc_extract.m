function [EXTRACT, RAFFINATE] = cc_extract(SOLVENT,MIX,unitCount)

%               _______
%  --- SOL --->|       |--- RAF --->
%              |       |  
%              |       |    
%  <-- EXT ----|_______|<-- MIX ----
%
% ECTRACT, RAFFINATE,SOLVENT,MIX are all streams with
% components [H2O; VAM; HAc] in mass flow rate


streamCount = 2*unitCount+2;

streams = zeros(3,streamCount);

streams(:,1) = SOLVENT;
streams(:,end) = MIX;


for j = 1:3*streamCount

newStreams = zeros(size(streams));
newStreams(:,1) = SOLVENT';
newStreams(:,end) = MIX';

for i = 1:unitCount

    [newStreams(:,2*i+1),newStreams(:,2*i)] = ...
        extract(streams(:,2*i-1) + streams(:,2*i+2));

end

streams = newStreams;
end

EXTRACT = streams(:,2);
RAFFINATE = streams(:,streamCount-1);