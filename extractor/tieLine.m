function [x,y] = tieLine(xBOUNDARY,yBOUNDARY,TIE_LINES,xF,yF);

% Gives the composition of all phases for a given composition [x0,y0],
% phase boundary, and tie lines y = [x,b].
%
% VAM Rich phase on left, WATER rich phase on right
%
%
% Is the composition in one phase?

% Expecting xF yF to be in terms of concentration, need them in tern
% coordinates. Will have to change them back af the end.

[xF,yF] = rect2tern(xF,yF);

% y_bound = interp1(xBOUNDARY,yBOUNDARY,xF);
% 
% if (yF >= y_bound || isnan(y_bound) )
%     
%     x = [xF;xF];
%     y = [yF;yF];
%     
%     [x,y] = tern2rect(x,y);
%     return
% end
% 
% clear y_bound;

% It's in the two phase region, let's figure our the slope

for i = 1:size(TIE_LINES,1)
    y_tie(i) = polyval(TIE_LINES(i,:),xF); 
end

m = interp1(y_tie,TIE_LINES(:,1),yF);

% Get it in polynomial form
line = [m, yF - m*xF];



[~,maxINDEX] = max(yBOUNDARY);
maxINDEX = maxINDEX-1;


R_Bound = @(x) interp1( xBOUNDARY(maxINDEX:end),...
                        yBOUNDARY(maxINDEX:end),...
                        x);
                    
L_Bound = @(x) interp1( xBOUNDARY(1:maxINDEX),...
                        yBOUNDARY(1:maxINDEX),...
                        x);
try             
xR = fzero(@(x)(polyval(line,x) - R_Bound(x)),...
                [xBOUNDARY(maxINDEX),xBOUNDARY(end)]);
yR = polyval(line,xR);            
            
xE = fzero(@(x)(polyval(line,x) - L_Bound(x)),...
                [xBOUNDARY(1),xBOUNDARY(maxINDEX)]);
yE = polyval(line,xE);
catch
   disp('ERROR in tieLine. fzero had no solution.')
   disp('Possibly no phase seperation, or near plait point.') 
   disp(' ')
   
   xE = xF;
   yE = yF;
   xR = xF;
   yR = yF;
   
end

x = [xE;xR];
y = [yE;yR];

[x,y] = tern2rect(x,y);