%% Acetic Acid, VAM, Water Extraction
clc, clear
run('prep.m')


% TODO: Break this script down into smaller, reusable functions

% TODO: Incorperate flow rates and mass distributions

%%  Read and configure data
[xBOUNDARY, yBOUNDARY, TIE_LINE] = JulianSmithTernaryData();

%% OUTPUT

A_title = 'Weight Percent Acetic Acid';
A_abv = 'HAc';
B_title = 'Weight Percent Water';
B_abv = 'H20';
C_title = 'Weight Percent Vinyl Acetate';
C_abv = 'VAM';

hold on
ternPlot(   A_title,A_abv,...
            B_title,B_abv,...
            C_title,C_abv)
        
plot(xBOUNDARY,yBOUNDARY,'k')


load('OUTPUT.mat')
input1 = findStream(streams,'TOPS');
input2 = findStream(streams,'ABS LIQ');

streams = [streams(input1), streams(input2)];

for i = 1:length(streams)
    
    % Need to convert stream from mole to mass frac
    % 
    % Get total mass
    
    Totmass = moles2mass('Water',streams(i).MOLES*streams(i).Water) + ...
            moles2mass('HAc',streams(i).MOLES*streams(i).HAc) + ...
            moles2mass('VAM',streams(i).MOLES*streams(i).VAM);
    
    % Get mass frac
    massFracWater = moles2mass('Water',streams(i).MOLES*streams(i).Water)/Totmass;
    massFracHAc = moles2mass('HAc',streams(i).MOLES*streams(i).HAc)/Totmass;
    
    % Carry on...
    [x0,y0] = tern2rect(massFracWater, massFracHAc);
    [x,y]=tieLine(xBOUNDARY,yBOUNDARY,TIE_LINE,x0,y0);
    [x,y] = rect2tern(x,y);
    [x0,y0] = rect2tern(x0,y0);
    plot(x0,y0,'or')
    plot(x,y,'r')
end
