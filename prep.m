function prep()

% Run this inside the Design 1 folder
%
% This adds all folders and subfolders in the current directory
% to the current search path.
addpath(genpath(cd))
disp('All subdirectories added to search path!')