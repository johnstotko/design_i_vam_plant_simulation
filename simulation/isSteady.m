function [isTrue,ACC] = isSteady(streams)
% Checks if all components have reached steady state

comps = getCompList();

ACC = zeros(1:length(comps));
isTrue = zeros(1:length(comps));

for i = 1:length(comps)
    
   [~,ACC(i)] = isCompAccumulating(streams,comps{i});   
    
end

isTrue = all(isTrue);