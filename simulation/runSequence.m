%% SIMULATION RUN SEQUENCE


for i = 1:stepGap
%% YELLOW BLOCK
streams = ReactorFeedAssuranceBlock(mixers(1),reactors(1),streams);
[valves(1),     streams] = runValve(valves(1), streams);
[valves(2),     streams] = runValve(valves(2), streams);
[turbines(1),   streams] = runTurbine(turbines(1), streams);
[mixers(1),     streams] = runMixer(mixers(1),streams);
[heaters(1),    streams] = runHeater(heaters(1),streams);
[reactors(1),   streams] = runReactor(reactors(1),streams);
[reactors(1),   streams] = ReactorCWSBlock(reactors(1),streams);
[valves(3),     streams] = runValve(valves(3), streams);
[coolers(1),    streams] = runCooler(coolers(1),streams);
[flashers(1),   streams] = runFlashDrum(flashers(1),streams);
[turbines(3),   streams] = runTurbine(turbines(3), streams);
[heatxs(1),     streams] = Col1HeatXAssuranceBlock(heatxs(1), streams);
[heatxs(1),     streams] = runHeatX(heatxs(1),streams);

%% BLUE BLOCK
[columns(1),    streams] = runColumn1(columns(1), streams); 
[pumps(1),      streams] = runPump(pumps(1), streams);
[decanters(1),  streams] = runDecanter(decanters(1),streams);
[pumps(2),      streams] = AbsorberLiqPAssuranceBlock(pumps(2),streams);
[pumps(2),      streams] = runPump(pumps(2), streams);
[turbines(2),   streams] = AbsorberCWSPAssuranceBlock(turbines(2), streams);
    for j = 1:2
    [turbines(2),   streams] = runTurbine(turbines(2),streams);
    [mixers(2),     streams] = runMixer(mixers(2), streams);
    [absorbers(1),  streams] = runAbsorber(absorbers(1),streams);
    end
[turbines(4),   streams] = runTurbine(turbines(4), streams);    
[splitters(1),  streams] = runSplitter(splitters(1),streams);
[compressors(1),streams] = runCompressor(compressors(1), streams);
end

%% GREEN BLOCK
[decanters(2),  streams] = runDecanter(decanters(2), streams);
[mixers(3),     streams] = runMixer(mixers(3), streams);
[coolers(2),    streams] = runCooler(coolers(2),streams);
[columns(2),    streams] = runColumn2(columns(2), streams);