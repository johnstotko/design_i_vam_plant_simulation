streams(end+1) = newStream('Name','ET FEED V');
streams(end+1) = newStream('Name','O2 FEED V');
streams(end+1) = newStream('Name','AC FEED V');
streams(end+1) = newStream('Name','FEED MIX');
streams(end+1) = newStream('Name','RXN FEED');
streams(end+1) = newStream('Name','RXN HOT');
streams(end+1) = newStream('Name', 'CWS 2',...
                            'T', 32.2222,... % 90 F
                            'P', 4.8159,... % 65 psig
                            'Water', 1);
streams(end+1) = newStream('Name','CWS 2 HOT',...
                            'Water',1);
streams(end+1) = newStream('Name','CWS 2 COOL');
streams(end+1) = newStream('Name','CWS 2 COLD');
streams(end+1) = newStream('Name','RXN HOT V');
streams(end+1) = newStream('Name','RXN COND');
streams(end+1) = newStream('Name','RXN VAP');
streams(end+1) = newStream('Name','RXN LIQ');
streams(end+1) = newStream('Name','RXN LIQ P');
streams(end+1) = newStream('Name','COL 1 FEED');
streams(end+1) = newStream('Name','TOPS');
streams(end+1) = newStream('Name','BOTS 1');
streams(end+1) = newStream('Name','HAC RECYCLE');
streams(end+1) = newStream('Name','ABS LIQ');
streams(end+1) = newStream('Name','ABS LIQ P');
streams(end+1) = newStream('Name','WASTE WATER');
streams(end+1) = newStream('Name','RAFFINATE');
streams(end+1) = newStream('Name','RAFFINATE P');
streams(end+1) = newStream('Name','EXTRACT');
streams(end+1) = newStream('Name','EXTRACT 2');
streams(end+1) = newStream('Name', 'CWS',...
                            'T', 32.2222,... % 90 F
                            'P', 4.8159,... % 65 psig
                            'Water', 1);
streams(end+1) = newStream('Name', 'CWS-T');
streams(end+1) = newStream('Name','ABS WASH');
streams(end+1) = newStream('Name','C-VAP');
streams(end+1) = newStream('Name','C-VAP-P');
streams(end+1) = newStream('Name','PURGE');
streams(end+1) = newStream('Name','ET REC');
streams(end+1) = newStream('Name','EXTRACT MIX');
streams(end+1) = newStream('Name','COL 2 FEED');
streams(end+1) = newStream('Name','BOTS 2');
streams(end+1) = newStream('Name','PRODUCT');
