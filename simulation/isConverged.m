function [isTrue, ACC] = isConverged(streams, tol)
% isTrue = isConverged(streams)

comp = getCompList;
ACC = zeros(1,length(comp));

for i = 1:length(comp)
    [~,ACC(i)] = isCompAccumulating(streams,comp{i},tol);
end

ACC = sum(ACC);

if ACC < tol
    isTrue = true;
else
    isTrue = false;
end


