toCommandQuery = input('Do you want to view results in the CommandWindow? ([Y]/N)\n','s');

if isempty(toCommandQuery)
    toCommandQuery = 'Y';
end

if strcmp(toCommandQuery,'Y')
    
massOrMoleQuery = input('Display streams in terms of mass or moles? (MASS/MOLES/[BOTH])\n','s');

if isempty(massOrMoleQuery)
    massOrMoleQuery = 'BOTH';
end

% Streams
disp('STREAMS (all material rates are per second)')

if strcmp(massOrMoleQuery,'MASS') || strcmp(massOrMoleQuery,'BOTH')
    dispMASS(streams)
    disp(' ')
    disp(' ')
end

if strcmp(massOrMoleQuery,'MOLES') || strcmp(massOrMoleQuery,'BOTH')
    dispMOLES(streams)
    disp(' ')
    disp(' ')
end


dispUnitQuery = input('Display unit specs? (Y/[N])\n','s');

if isempty(dispUnitQuery)
    dispUnitQuery = 'N';
end


if strcmp(dispUnitQuery,'Y')
% Mixers
disp('MIXER UNITS')
disp(struct2table(mixers))

disp(' ')
disp(' ')

% Flashers
disp('FLASH UNITS')
disp(struct2table(flashers))
disp(' ')
disp(' ')

% Decanters
disp('DECANTERS')
disp(struct2table(decanters))
disp(' ')
disp(' ')

% Heater
disp('HEATER UNITS')
disp(struct2table(heaters))
disp(' ')
disp(' ')

% Heat X
disp('HEAT-X UNITS')
disp(struct2table(heatxs))
disp(' ')
disp(' ')

% Reactor
disp('REACTORS')
disp(struct2table(reactors))
disp(' ')
disp(' ')

% Column
disp('COLUMNS')
disp(struct2table(columns))
disp(' ')
disp(' ')
end
end