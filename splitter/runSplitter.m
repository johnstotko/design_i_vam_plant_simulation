function [unit, streams] = runSplitter(unit, streams)

% Get stream indexes
Input = findStream(streams,unit.Input);
Output1 = findStream(streams,unit.Output1);
Output2 = findStream(streams,unit.Output2);

comps = getCompList;

% Split MOLES. 
streams(Output1).MOLES = (1-unit.SplitFrac)*streams(Input).MOLES;
streams(Output2).MOLES = unit.SplitFrac*streams(Input).MOLES;

% THE FIRST STREAM
if streams(Output1).MOLES ~= 0
% Molar composition is the same
for i = 1:length(comps)
    streams(Output1).(comps{i}) = streams(Input).(comps{i});
end

% Temperature should be the same
    streams(Output1).T = streams(Input).T;
    
% Pressure Drop
    streams(Output1).P = streams(Input).P - unit.Pressure_Drop;
end

% THE SECOND STREAM
if streams(Output2).MOLES ~= 0    
% Molar composition is the same
for i = 1:length(comps)
    streams(Output2).(comps{i}) = streams(Input).(comps{i});
end

% Temperature should be the same
    streams(Output2).T = streams(Input).T;
    
% Pressure Drop
    streams(Output2).P = streams(Input).P - unit.Pressure_Drop;
end

