function unit = newSplitter(varargin)

unit = struct(  'Name', [],...
                'Input', [],... Stream name
                'Output1', [],... Stream name
                'Output2', [],... Stream name
                'SplitFrac', 0,...
                'Pressure_Drop', 0);
            
for i = 1:2:nargin
    unit.(varargin{i}) = varargin{i+1};   
end