function [molScale, X] = eps2molfrac(X0,v,eps)

% X(1)  Ethylene
% X(2)  Acedic Acid
% X(3)  Oxygen
% X(4)  VAM
% X(5)  Water
% X(6)  Carbon Dioxide

% X0    Initial conditions for all species
molScale = 1 - 0.5*eps(1);

X = (X0 + v'*eps)./(molScale);