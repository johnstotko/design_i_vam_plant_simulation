function v = de2(e1,e2,p,l,var)
% v = de2(e1,e2,p,var)
% 
%1  var = [Dp;
%2          voidFrac;
%3          pCat;
%4          M0;
%5          unit.CrossSestionalArea;
%6          unit.RetentionTime;
%7          vis;
%8          streams(Input).T;
%9          streams(Input).MOLES;
%10         streams(Input).ET ;
%11         streams(Input).O2 ];

A = var(3)*var(6)*var(5);
B = k(2,var(8))/(2*var(9));
C = p^(alpha(2,var(8))+beta(2,var(8)));
D = ((var(10)-e1-e2)/(1-(0.5*e1)))^alpha(2,var(8));
E = ((var(11)-(0.5*e1)-(3*e2))/(1-(0.5*e1)))^beta(2,var(8));

v = A*B*C*D*E;