function b = beta(n,T)
% Temperature dependence later...

 switch n
    case 1,
        b = 0.20; % Average for now
    case 2,
        b = 0.86; % Average for now
    otherwise,
        error('n must be 1 or 2')
 end
    
end

