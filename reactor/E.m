function e = E(n,T)

% T     Temperature in Celsius
%
% Only valid for temperatures between 120 and 160
switch n
    case 1
        if ((T<=140) && (T>=120))
            e = 40000; % kJ
        elseif ((T<=160) && (T>140))
            e = 15000; % kJ
        else
            error('Temperatire must be between 140 and 160 C')
        end
    case 2
        e = 21000;  % kJ
    otherwise
        error('n must be 1 or 2')
end
