function [unit, streams] = runReactor(unit,streams)


% PACKING PARAMETERS

Dp = 5e-3; % Diameter of packing, meter
voidFrac = 0.3; % packing void fraction
pCat = 560.646; % kg per cubic meter


% Input/Output Index
Input = findStream(streams,unit.Input);
Output = findStream(streams,unit.Output);


comps = getCompList();
% Get the mass of the input stream
components = zeros(1,length(comps));

% Get all the molar flows [mol]
for i = 1:length(comps)
components(i) = [streams(Input).(comps{i})].*[streams(Input).MOLES];
end

% Convert to mass [kg]
M0 =  sum(components(:).*molarMass(comps)')/1000;

% Get viscosity
vis = getStreamViscosity(streams(Input))*(10^-6);

var = [Dp;...
        voidFrac;...
        pCat;...
        M0;...
        unit.CrossSectionalArea;...
        unit.RetentionTime;...
        vis;...
        streams(Input).T;...
        streams(Input).MOLES;...
        streams(Input).ET ;...
        streams(Input).O2]; % Constant parameters for the diff eq
    
O2frac = @(eps) (streams(Input).O2 - 0.5*eps(1) - 3*eps(2))/(1 - 0.5*eps(1));
    
df = @(l,Y) [de1(Y(1),Y(2),Y(3),l,var);
             de2(Y(1),Y(2),Y(3),l,var);
              dp(Y(1),Y(2),Y(3),l,var)];

L = 0;
O2 = streams(Input).O2;

while O2 > unit.TargetO2Frac
    
L = L+0.01;
[~,Y] = ode45(@(l,Y)df(l,Y),[0,L],[0;0;streams(Input).P]);

O2 = O2frac(Y(end,1:2));
end

eps = Y(end,1:2)*streams(Input).MOLES; % Un-nondimentionalize extents
Pout = Y(end,3);

%% PUT IN OUTPUT STREAM INFORMATION

            %  ET    O2    HAc   VAM  Water   CO2  C1   C2   Ar   N2
stoichCoeff = [-1   -0.5   -1    1    1       0    0    0    0    0;
               -1   -3      0    0    2       2    0    0    0    0];
       
% Put in total mole information first (not mole fraction)
streams(Output).MOLES = 0;
for i = 1:length(comps)
    % Initial moles of component
    n0 = streams(Input).(comps{i})*streams(Input).MOLES;

    % Final moles of component
    streams(Output).(comps{i}) = n0 + eps*stoichCoeff(:,i);
    
    % Aggregate moles for stream
    streams(Output).MOLES = streams(Output).MOLES + ...
                                streams(Output).(comps{i});
                                                 
end
 
% Now MOLES should be correct, let's convert components from moles/s to
% moles fractions
for i = 1:length(comps)
    
    streams(Output).(comps{i}) = streams(Output).(comps{i})/...
                                    streams(Output).MOLES;
end

% Then put P and T
streams(Output).P = Pout;
streams(Output).T = streams(Input).T;


%% PUT IN UNIT INFORMATION

unit.CatalystWeight = L*unit.CrossSectionalArea*pCat;
unit.Duty = eps(1)*179.912*3600*10^-6; %43 kcal = 179.912 kJ... MM kJ/hr
unit.Length = L;

