function a = A(n,T)

switch n
    case 1
        a = 9.7*10^-3;
    case 2
        a = 5.13*10^-4;
    otherwise
        error('n must be 1 or 2')
end
