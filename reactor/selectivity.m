function s = selectivity(ext)
% Selectivity of oxygen consumption
s = 0.5*ext(1)/(0.5*ext(1) + 3*ext(2));