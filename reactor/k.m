function k = k(i,T)
% k = k(i,mol0,T)
%
% i     ith reaction
% mol0  initial molar flowrate
% T     Temperature in Celsius
     
R = 8.314; %J/molK

k = A(i,T)*exp(-E(i,T)/(R*(T+273)));

