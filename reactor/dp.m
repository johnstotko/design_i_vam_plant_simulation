function v = dp(e1,e2,p,l,var)
% v = dp(e1,e2,p,var)
% 
%1  var = [Dp;
%2          voidFrac;
%3          pCat;
%4          M0;
%5          unit.CrossSestionalArea;
%6          unit.RetentionTime;
%7          vis;
%8          streams(Input).T;
%9          streams(Input).MOLES;
%10         streams(Input).ET ;
%11         streams(Input).O2 ;

A = (var(4))/(var(1)*var(6));
B = (1-var(2))/((var(2))^3);
C = (150*(1-var(2))*var(7))/(var(4)*var(1));

v = -2*l*A*B*(C + 1.75)*(10^-5);