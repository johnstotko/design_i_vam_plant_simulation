function unit = newReactor(varargin)

unit = struct(  'Name', [],...
                'Input', [],... Stream name
                'Output', [],... Stream name
                'TargetO2Frac', 0,...
                'TargetFeed', 0,...
                'CrossSectionalArea', 1,... % square meters
                'RetentionTime', 60,... $ seconds
                'CatalystWeight', 0,... $ kg
                'Length', 0,... % m
                'Duty', 0);
            
for i = 1:2:nargin
    unit.(varargin{i}) = varargin{i+1};   
end