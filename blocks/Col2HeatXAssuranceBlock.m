function [unit, streams] = Col2HeatXAssuranceBlock(unit, streams)
% [unit, streams] = Col2HeatXAssuranceBlock(unit, streams)
%
% Changes the cold approach temp in heatX 1 so that col 1 feed is at the
% buuble temp.

ColdIn = findStream(streams,'EXTRACT MIX');
HotIn = findStream(streams, 'CWS 2 COOL');

Qin = streams(ColdIn).MOLES*...
            getStreamCp(streams(ColdIn))*...
            (75.7 - streams(ColdIn).T);
        
 
Qout = @(t) streams(HotIn).MOLES*...
            (H('Water',t,streams(HotIn).P) - ...
            H('Water',streams(HotIn).T,streams(HotIn).P))*1000;
        
f = @(t) Qin + Qout(t);

T_HotOutput = fzero(@(t)f(t),streams(HotIn).T);

unit.ColdApproach = T_HotOutput - streams(ColdIn).T;
