function [unit, streams] = ProductionAssuranceBlock(streams, unit, tol, gain)
% [unit, streams] = ProductionAssuranceBlock(streams, unit, tol)
% 
% Adjusts the target feed for the reactor to meet production spec.
%
% Operates as a simple porportional controller, with just a gain setting.
%
% The gain is arbritraty, but shouldn't be "too big".

[isTrue, diff] = isProduction200kta(streams, tol);

if ~isTrue
   
    unit.TargetFeed = unit.TargetFeed + gain*diff;
    
end
    