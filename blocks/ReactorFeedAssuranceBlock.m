function [streams] = ReactorFeedAssuranceBlock(mixer, reactor, streams)
%
% This block garuntees that the output of mixer1 matches reactor specs:
%
% i.e. 
%
% O2 mole fraction is 7%
% 
% Et = 3*HAc

%% CONTROL STREAMS

O2 = findStream(streams,'O2 FEED');
ET = findStream(streams,'ET FEED');
HA = findStream(streams,'AC FEED');

%% OTHER STREAMS
%
% ID the other streams

OtherStreamNames = {};
for i = 1:length(mixer.Inputs)
    if ~strcmp(mixer.Inputs{i},'O2 FEED V') && ...
             ~strcmp(mixer.Inputs{i},'ET FEED V') && ...
             ~strcmp(mixer.Inputs{i},'AC FEED V')
       
          OtherStreamNames =  [OtherStreamNames, mixer.Inputs{i}];
          
    end
end
    
OTI = findStream(streams,OtherStreamNames); %Other Stream Indexes
                    


%% DEFINE TARGETS

TargetFlow = reactor.TargetFeed;
TargetO2Frac = 0.07;

%% CALCULATIONS
%
% Setting up a vector equation. Ax = b
%
% First row is total flow balance
% Second row is O2 balance
% Third row is HAc and ET balance/constraint
%
% The x vector is [ET; HA; O2], total flow rates 

A = [ 1  ,                  1  ,                    1 ; 
      streams(ET).O2  ,  streams(HA).O2   , streams(O2).O2  ;
      streams(ET).ET - 3*streams(ET).HAc   ,  streams(HA).ET - 3*streams(HA).HAc   , streams(O2).ET - 3*streams(O2).HAc  ];

b = [ TargetFlow - sum([streams(OTI).MOLES]) ;
      TargetO2Frac*TargetFlow - sum([streams(OTI).O2].*[streams(OTI).MOLES]) ;
       -sum(([streams(OTI).ET] - 3*[streams(OTI).HAc]).*[streams(OTI).MOLES])];

x = A\b;


streams(ET).MOLES = x(1);
streams(HA).MOLES = x(2);
streams(O2).MOLES = x(3);





