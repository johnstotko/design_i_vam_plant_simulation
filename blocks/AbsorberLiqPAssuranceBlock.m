function [unit, streams] = AbsorberLiqPAssuranceBlock(unit, streams)
% changes the raffinate pump settings to that it matches the inlet vapor
% pressure to the absorber

RXN_VAP = findStream(streams,'RXN VAP');

unit.TargetPressure = streams(RXN_VAP).P;
