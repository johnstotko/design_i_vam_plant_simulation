function [unit, streams] = AbsorberCWSPAssuranceBlock(unit, streams)
% Changes the target pressure for the CWS turbine so that it matches
% RAFFINATE's pressure.

RAFFINATE = findStream(streams,'RAFFINATE P');

unit.TargetPressure = streams(RAFFINATE).P;