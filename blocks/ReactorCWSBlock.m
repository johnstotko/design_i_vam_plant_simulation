function [unit, streams] = ReactorCWSBlock(unit,streams)
% [unit, streams] = ReactorCWSBlock(unit,streams)

CWS = findStream(streams, 'CWS 2');
CWS_HOT = findStream(streams, 'CWS 2 HOT');

streams(CWS_HOT).T = 130;
streams(CWS_HOT).P = streams(CWS).P - 0.01; % Phase Change

dH = (H('Water',streams(CWS_HOT).T,streams(CWS_HOT).P) - ...
            H('Water',streams(CWS).T,streams(CWS).P));

streams(CWS).MOLES = unit.Duty/dH;
streams(CWS_HOT).MOLES = unit.Duty/dH;
