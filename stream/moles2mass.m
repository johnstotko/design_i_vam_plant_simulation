function mass = moles2mass(comp,moles)
% mass = moles2mass(comp,moles)
%
% Converts moles of component comp to mass in g
%
% Will accept comp as cell array and moles as array

mass = moles.*molarMass(comp);