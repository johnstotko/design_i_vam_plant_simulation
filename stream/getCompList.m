function comp = getCompList()
% Gices a cell array of component names

comp = {'ET',
        'O2',
        'HAc',
        'VAM',
        'Water',
        'CO2',
        'C1',
        'C2',
        'Ar',
        'N2'};