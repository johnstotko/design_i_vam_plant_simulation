function dispMASS(streamArray)

unitTag = '_kg';

comps = getCompList;

components = zeros(length(streamArray),length(comps));

% Get all the molar flows [mol/s]
for i = 1:length(comps)
components(:,i) = [streamArray.(comps{i})].*[streamArray.MOLES];
end

% Convert to mass [kg/hr]
for i = 1:length(streamArray)
components(i,:) =  components(i,:).*molarMass(comps)*3600/1000;
end

% Add in the temperature and pressure

components = [[streamArray.T]', [streamArray.P]', components];

% Put data in table
T = array2table(components);

% Rename the column
T.Properties.VariableNames = [{'T_C' 'P_Bar'} strcat(comps, unitTag)'];

% Rename the rows
T.Properties.RowNames = {streamArray.Name};

% Display the table
disp(T)
