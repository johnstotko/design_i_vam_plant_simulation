function index = findStream(streamArray, name)
% i = findStream(streamArray, name)
%
% This function makes outputs a copy of the stream in streamArray with Name
% = name.
%
% streamArray   An array of streams structs
% name          String. The name of the stream to look for
% i             If found, is the index of the stream with Name = name in
%                   streamArray
if ~iscell(name)
    name = {name};
end



index = [];
for j = 1:length(name)
    for i = 1:length(streamArray)
        if strcmp(streamArray(i).Name,name{j})
            index = [index, i];
            continue
        end
    end
end

if isempty(name) && isempty(index)
    
    names = [];
    
    for i = 1:length(name)
        names = [names , ' ', name{i}];
    end
    
   error(['Stream ' names ' not found']) 
end