function s = formatStreams(streams)
%s = newFormattedStream(stream)
% 
% Takes a stream in default format and generates a stream ready for
% outputting and display

for k = 1:length(streams)

s(k).Name = streams(k).Name;
s(k).T = streams(k).T;
s(k).P = streams(k).P;

comps = getCompList;

% Get the flow rate in kg/hr

for i = 1:length(comps)
    s(k).(comps{i}) =  streams(k).MOLES * streams(k).(comps{i}) * ...
                        molarMass(comps{i})/1000; % kg/s
    s(k).(comps{i}) = s(k).(comps{i})*3600;
end

end

