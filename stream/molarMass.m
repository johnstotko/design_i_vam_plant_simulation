function m = molarMass(comp)
% m = molarMass(comp)
%
% Gives the molar mass of comp in g/mol
%
% comp      component name, from getCompList
%
% If comp is a cell array, molarMass will be an array of mollar masses.

% If comp isn't a cell array, make it one
if ~iscell(comp)
    comp = {comp};
end


H = 1.00794; % NIST Hydrogen Atom [g/mol]
O = 15.9994; % NIST Oxygen, Atomic [g/mol]
C = 12.0107; % NIST Carbon [g/mol]

%m = zeros(size(comp));

for i = 1:length(comp)
switch comp{i}
    case 'ET'
        m(i) = 2*C + 4*H;
        continue
    case 'O2'
        m(i) = 2*O;
        continue
    case 'HAc'
        m(i) = 2*C + 4*H + 2*O;
        continue
    case 'VAM'
        m(i) = 4*C + 6*H + 2*O;
        continue
    case 'Water'
        m(i) = 2*H + O;
        continue
    case 'CO2'
        m(i) = C + 2*O;
        continue
    case 'C1'
        m(i) = C + 4*H;
        continue
    case 'C2'
        m(i) = 2*C + 6*H;
        continue
    case 'Ar'
        m(i) =  39.948; 
        continue
    case 'N2'
        m(i) = 14.0067;
        continue
end
end