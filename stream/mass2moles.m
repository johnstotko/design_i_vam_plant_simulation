function moles = mass2moles(comp,mass)
% moles = mass2moles(comp,mass)
%
% Converts mass in g of comp to moles
%
% Will accept comp as cell array and mass as array

moles = mass./molarMass(comp);