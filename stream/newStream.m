function s = newStream(varargin)
% Stream constructor
%
% Create struct and add basic properties
s = struct( 'Name', [],...
            'T',    0,...
            'P',    0,...
            'MOLES', 0);

% Add all the components, and initiate to 0
comps = getCompList();

for i = 1:length(comps)
s.(comps{i}) = 0;
end

% Put in specs
for i = 1:2:nargin
    s.(varargin{i}) = varargin{i+1};   
end

