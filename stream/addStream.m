function streams = addStream(streamArray, Name)
% Appends a new stream struct to streamArray
%
% streams   output stream array
streams = [streamArray, newStream(Name)];