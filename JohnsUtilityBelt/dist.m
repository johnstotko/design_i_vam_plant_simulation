function d = dist(x,y)
% Calculates the euclidian distance between n-tupples x and y
d = sqrt(sum((x - y).^2));