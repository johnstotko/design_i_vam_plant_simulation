function findTag(tag,outerDirectory)
% This function looks through the files in the specified folder and its 
% subfolders for the tag

lenTAG = length(tag);

% Little welcome message, because why not
disp('*******************************************************************')
disp(' ')
disp(['Here is what has been marked with:  ',tag])
disp(' ')
disp(' ')



% And displays them in the command window
list = findFileWithExt('.m',cell(0),outerDirectory)';

for i = 1:length(list)
    filename = list{i};
    text = fileread(filename);
    text = strsplit(text, '\n');

    for i = 1:length(text)
       line = text{i};

       if length(line) >= lenTAG      
           if strcmp(line(1:lenTAG), tag)
               disp(filename)
               disp(['* ',line(lenTAG+1:end)])
           end
       end
    end
end
disp(' ')
disp('*******************************************************************')