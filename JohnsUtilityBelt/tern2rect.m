function [x,y] = tern2rect(x1,y1)

y = x1 - (1/sqrt(3))*y1;
x = x1 + (1/sqrt(3))*y1;
x = 1-x;