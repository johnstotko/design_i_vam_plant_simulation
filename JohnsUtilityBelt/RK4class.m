function y = RKclass(f,t,y0)
%% PARAMETERS
% ONLY FIRST ORDER EQUATIONS
% f     dy = f(t,y)
% t     array of t values
% y0    array with initial conditions

A = [0 0 0 0;0.5 0 0 0; 0 0.5 0 0; 0 0 1 0];
c = [0 0.5 0.5 1];
b = [1/6 1/3 1/3 1/6];

m = 4; % This is how many stages we have

y = zeros(length(y0),length(t));
y(:,1) = y0;

for n = 1:length(t)-1 % n is the time index

        % CALCULATE STAGES
        dY = zeros(length(y0),m);
        dt = t(n+1) - t(n);

        for i = 1:m % i tracks the RK stage index
            dY(:,i) = f(t(n) + c(i)*dt, ...
                        y(:,n) + dt*(dY*A(i,:)'));
        end
        % CALCULATE NEXT Y-VALUE
        y(:,n+1) = y(:,n) + dt*dY*b';
end