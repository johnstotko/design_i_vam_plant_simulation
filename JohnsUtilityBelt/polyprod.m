function y = polyprod(x1,x2)
% Multiplies the two row vectors x1 and x2 as if 
% they were polynomials.

n = length(x1);
m = length(x2);

r = [ x2   , zeros(1,n-1)];
c = [ x2(1), zeros(1,m+n-2)];

M = toeplitz(c,r);
M = M(1:n,:);

y = x1*M;
