function string = log2str(log)
% string = log2str(log)
%
% Converts logical variable to 'true' if log == 1, or 'false' or log == 0
if ~islogical(log)
   error('Input must be type logical'); 
end

if log
    string = 'PASS';
else
    string = 'FAIL';
end