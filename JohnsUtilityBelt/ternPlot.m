function ternPlot(A_title,A_abv,B_title,B_abv,C_title,C_abv)
% ternPlot(A_title,A_abv,B_title,B_abv,C_title,C_abv)
%
% Displays a ternary diagram
%
%      A
%     / \
% C  /   \  A
%   /     \
%  C-------B
%      B


hold on

% This is the border
plot([0,1,.5,0],[0,0,sqrt(3)/2,0],'k')

% this is the grid

gridcolor= [0.7,0.7,0.7];

%% A

text(0.5,0.9,A_abv,'HorizontalAlignment','center')

% Small grids
for i = 1:9
    x1 = 0.05*i;
    x2 = 1-x1;
    y1 = (sqrt(3)/20)*i;
    y2 = y1;
    plot([x1, x2], [y1, y2], 'Color', gridcolor);
    
    text(x2+0.015,y2+0.015,num2str(i*10),'HorizontalAlignment','center')
end

text(0.75+0.05,sqrt(3)/4 + 0.05,A_title,...
    'HorizontalAlignment','center',...
    'Rotation',-60)


%% B

text(1.02,-0.02,B_abv,'HorizontalAlignment','center')

% Small grids
for i = 1:9
    x1 = 0.1*i;
    x2 = (1 + 0.1*i)/2;
    y1 = 0;
    y2 = -sqrt(3)*(x2 - 1);
    plot([x1, x2], [y1, y2], 'Color', gridcolor);
    
    text(x1,y1-0.015,num2str(i*10),'HorizontalAlignment','center')
end

text(0.5,-0.05,B_title,'HorizontalAlignment','center')

%% C

text(-0.02,-0.02,C_abv,'HorizontalAlignment','center')

% Small grids
for i = 1:9
    x1 = 0.1*i;
    x2 = 0.1*i/2;
    y1 = 0;
    y2 = sqrt(3)*x2;
    plot([x1, x2], [y1, y2], 'Color', gridcolor);
    
    text(x2-0.015,y2+0.015,num2str((10-i)*10),'HorizontalAlignment','center')
end

text(0.25-0.05,sqrt(3)/4 + 0.05,C_title,...
    'HorizontalAlignment','center',...
    'Rotation',60)


axis equal
axis off






