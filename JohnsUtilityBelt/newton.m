function n = newton(n, f)
% n = newton(n, f)
%
% Performs one newton-rhapson itteration on the functino f on starting
% value n.
h = 0.001;
df = @(x) (f(x+h) - f(x-h))/(2*h);

n = n - f(n)/df(n);