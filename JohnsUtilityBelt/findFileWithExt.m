function newList = findFileWithExt(ext,oldList,outerDirectory)
% Gets a list of .m files in a folder and its subfolders
addpath(outerDirectory);
run('prep.m')

list = ls;

for i =1:size(list,1)
line = cellstr(list(i,:));   

if strcmp(line{1},'.')
    continue
end
    
if strcmp(line{1},'..')
    continue
end 

if isempty(strfind(line{1},'.'))
    currentDirectory = pwd;
    cd(line{1});
    oldList = findFileWithExt(ext,oldList,outerDirectory);
    cd(currentDirectory);
    continue
end


if ~isempty(strfind(line{1},ext))
    oldList(length(oldList) + 1) = line;
end

end

newList = oldList;