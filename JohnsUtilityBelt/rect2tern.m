function [xt,yt]=rect2tern(xr,yr)
% This function maps rectilniear coordinates to ternary diagram coordinates

xr = 1-xr;
xt = (xr + yr)/2;
yt = -sqrt(3)*(xt - xr);