function L = lagrangepoly(x,y)
%
% x,y are vectors containing data

L = zeros(1,length(x));

for i = 1:length(x)
    
    xt = x;
    xt(i) = [];
    
    yt = y;
    yt(i) = [];
    
    P = y(i);
    
    for j = 1:length(xt)
        
       P = polyprod(P,[1, -xt(j)])/(x(i)-xt(j));
        
    end
    
    L = L+P;
    
end