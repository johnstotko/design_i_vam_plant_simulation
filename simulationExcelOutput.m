    disp(' ')
    disp(' ')
    filename = 'Process_Simulation.xlsx';
    disp('Saving streams...') 
    writetable(struct2table(streams),filename,'Sheet','streams');
    
    disp('Saving mixers...') 
    writetable(struct2table(mixers),filename,'Sheet','mixers');

    disp('Saving compressors...') 
    writetable(struct2table(compressors),filename,'Sheet','compressors');
    
    disp('Saving pumps...') 
    writetable(struct2table(pumps),filename,'Sheet','pumps');
    
    disp('Saving turbines...') 
    writetable(struct2table(turbines),filename,'Sheet','turbines');
    
    disp('Saving valves...') 
    writetable(struct2table(valves),filename,'Sheet','valves');
    
    disp('Saving flashers...') 
    writetable(struct2table(flashers),filename,'Sheet','flashers');
    
    disp('Saving decanters...') 
    writetable(struct2table(decanters),filename,'Sheet','decanters');
    
    disp('Saving heaters...') 
    writetable(struct2table(heaters),filename,'Sheet','heaters');
    
    disp('Saving absorbers...')
    writetable(struct2table(absorbers),filename,'Sheet','absorbers');
    
    disp('Saving heatxs...') 
    writetable(struct2table(heatxs),filename,'Sheet','heatxs');
    
    disp('Saving reactors...') 
    writetable(struct2table(reactors),filename,'Sheet','reactors');
    
    disp('Saving columns...') 
    writetable(struct2table(columns),filename,'Sheet','columns');
    
        disp('Saving coolers...') 
    writetable(struct2table(coolers),filename,'Sheet','coolers');
    
    disp('Finished!')