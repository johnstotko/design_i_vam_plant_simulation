function [unit, streams] = runHeater(unit,streams)

% GET Stream Indexes
Output = findStream(streams, unit.Output);
Input = findStream(streams, unit.Input);

% Set Streams
streams(Output) = streams(Input);
streams(Output).Name = unit.Output;
streams(Output).T = unit.TargetTemp;
streams(Output).P = streams(Output).P - unit.Pressure_Drop;

unit.Duty = (getStreamH(streams(Output)) - getStreamH(streams(Input)))*...
            3600*10^-6;% MM kJ/hr