function unit = newHeater(varargin)

unit = struct(  'Name', [],...
                'Input', [],... Stream name
                'Output', [],... Stream name
                'TargetTemp', 0,...
                'Duty', 0,... 
                'Pressure_Drop', 0);
            
for i = 1:2:nargin
    unit.(varargin{i}) = varargin{i+1};   
end