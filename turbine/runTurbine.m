function [unit, streams] = runTurbine(unit, streams)

% Get stream indexes
Input = findStream(streams, unit.Input);
Output = findStream(streams, unit.Output);

if streams(Input).MOLES ~= 0

streams(Output).P = unit.TargetPressure;
streams(Output).T = streams(Input).T;


% Composition/Mole Flow is identical
streams(Output).MOLES = streams(Input).MOLES;
comps = getCompList;
for i = 1:length(comps)
   streams(Output).(comps{i}) = streams(Input).(comps{i}); 
end
end