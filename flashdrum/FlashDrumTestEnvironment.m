%% FLASH DRUM TEST ENVIRONMENT
clc, clear
%% Define/Initiate Streams

% This is consistant with the one on the PFD Fleischer graded. This is
% useful since he plugged those numbers into Aspen.

streams(1) = newStream('Name', 'Input',...
                       'P', 1,...
                       'T', 66,...
                       'MOLES',860.3578192,...
                       'ET', 0.6176364342,...
                       'O2', 0.02581576223,...
                       'HAc', 0.1445917602,...
                       'VAM', 0.09247148264,...
                       'Water', 0.09434295054,...
                       'CO2', 0.01777526475,...
                       'C1', 0.0007415974759,...
                       'C2', 0.00173039411,...
                       'Ar', 0.003915483109,...
                       'N2', 0.0009788707773);

% streams(1) = newStream('Name', 'Input',...
%                         'P', 4,...
%                         'T', 30,...
%                         'MOLES', 1119.45,...
%                         'VAM', 0.0685,...
%                         'HAc', 0.1325,...
%                         'Water', 0.0753,...
%                         'O2', 0.0181,...
%                         'ET', 0.5983);
                        
streams(end+1) = newStream('Name', 'Vapor');
streams(end+1) = newStream('Name', 'Liquid');

%% Initiate Unit

flashers(1) = newFlashDrum('Name','Flash',...
                            'Input', 'Input',...
                            'Vapor','Vapor',...
                            'Liquid','Liquid');
                        
%% Run

[flashers(1), streams] = runFlashDrum(flashers(1), streams);

%% Output
disp(struct2table(streams))
dispMOLES(streams)

