function [unit, streams] = runFlashDrum(unit,streams)

% Get input/output index
Input = findStream(streams, unit.Input);
Vapor = findStream(streams, unit.Vapor);
Liquid = findStream(streams, unit.Liquid);

if streams(Input).MOLES ~= 0

% Temperature should be the same
streams(Vapor).T = streams(Input).T;
streams(Liquid).T = streams(Input).T;

% Pressure drop
streams(Vapor).P = streams(Input).P - unit.Pressure_Drop;
streams(Liquid).P = streams(Input).P - unit.Pressure_Drop;

% Reset outputs
streams(Vapor).MOLES = 0;
streams(Liquid).MOLES = 0;


% Get component list
comps = getCompList();

% Put total moles in the fields for now, will convert to mole frac later
for i = 1:length(comps) 
    if ~strcmp(comps{i},'VAM') && ...
            ~strcmp(comps{i},'Water') &&...
            ~strcmp(comps{i},'HAc')
        
        streams(Vapor).(comps{i}) = ...
            streams(Input).(comps{i})*streams(Input).MOLES;         
    end  
end


VF = vaporFraction(streams(Input));

zi = [0,0,0]; % [VAM, HAc, Water]
 
zi(1) = streams(Input).VAM;
zi(2) = streams(Input).HAc;
zi(3) = streams(Input).Water;

Ki = [getK(streams(Input),'VAM'),...
      getK(streams(Input),'HAc'),...
      getK(streams(Input),'Water')];

L = streams(Input).MOLES*(1-VF);

streams(Liquid).VAM =   (zi(1)/(1 + VF*(Ki(1) - 1)))*L;
streams(Liquid).HAc =   (zi(2)/(1 + VF*(Ki(2) - 1)))*L;
streams(Liquid).Water = (zi(3)/(1 + VF*(Ki(3) - 1)))*L;

streams(Vapor).VAM = streams(Input).VAM*streams(Input).MOLES -...
                            streams(Liquid).VAM;    
streams(Vapor).HAc = streams(Input).HAc*streams(Input).MOLES -...
                            streams(Liquid).HAc;
streams(Vapor).Water = streams(Input).Water*streams(Input).MOLES -...
                            streams(Liquid).Water;

% Convert liquid to MOLES
for i = 1:length(comps)
    streams(Liquid).MOLES = streams(Liquid).MOLES...
                            + streams(Liquid).(comps{i});
end

% Convert vapor to MOLES
for i = 1:length(comps)
    streams(Vapor).MOLES = streams(Vapor).MOLES...
                            + streams(Vapor).(comps{i});
end


% Convert liquid to mole frac
if streams(Liquid).MOLES ~= 0
for i = 1:length(comps)
    streams(Liquid).(comps{i}) = streams(Liquid).(comps{i})...
                                    /streams(Liquid).MOLES;
end
end

% Convert vapor to mole frac
if streams(Vapor).MOLES ~= 0
for i = 1:length(comps)
    streams(Vapor).(comps{i}) = streams(Vapor).(comps{i})...
                                    /streams(Vapor).MOLES;
end
end

end



