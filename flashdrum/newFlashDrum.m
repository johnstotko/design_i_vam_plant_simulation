function unit = newFlashDrum(varargin)

unit = struct(  'Name', [],...
                'Input', [],... Stream name
                'Vapor', [],... Stream name
                'Liquid', [],... Stream name
                'Duty', 0,... 
                'Pressure_Drop', 0);
            
for i = 1:2:nargin
    unit.(varargin{i}) = varargin{i+1};   
end