function t = getTc(comp)
% t = Tc(comp)
%
% Gives the Critical Temperature in Celcius
%
% If comp is a cell array, Tc(comp) will give an array

if ~iscell(comp)
    comp = {comp};
end

t = zeros(1,length(comp));

for i = 1:length(comp)
switch comp{i}
    
    case 'VAM'
        t(i) = 252;
        
    case 'HAc'
        t(i) = 320;
        
    case 'ET'
        t(i) = 9.5;
        
    case 'Water'
        t(i) = 374;
        
    case 'O2'
        t(i) = -118.6;
        
    case 'CO2'
        
        t(i) = 31.10;
        
    case 'C1'
        t(i) = -82.6; 
        
    case 'C2'
        
        t(i) = 32.2777778;
        
    case 'N2'
        t(i) = -146.9;
        
    case 'Ar'
        t(i) = -189.3;
        
    otherwise
        error('Component not found')
end
end