function VF = vaporFraction(streams)
% VF = vaporFraction(streams)
%
% Accepts streams(i) as array, gives VF as array
%
% Assumes only VAM, HAc, and Water can condense

VF = zeros(1,length(streams));

for i = 1:length(streams)

%     if streams(i).T < bubbleT(streams(i)) 
%         VF(i) = 0;
%     elseif streams(i).T > dewT(streams(i))
%         VF(i) = 1;
%     end
    
    % Get the z's
    zi = [streams(i).VAM, streams(i).HAc, streams(i).Water];
    
    % Get the Ki's in an array 
    % Ki = Psat({'VAM', 'HAc', 'Water'},streams(i).T)/streams(i).P;
    
    % Let's add in the activity coefficient
    Ki(1) = getK(streams(i), 'VAM');
    Ki(2) = getK(streams(i), 'HAc');
    Ki(3) = getK(streams(i), 'Water');
    
    % Set up Rachford-Rice
    f = @(vf) (zi(1).*(Ki(1) - 1))./(1 + vf.*(Ki(1) - 1)) + ...
              (zi(2).*(Ki(2) - 1))./(1 + vf.*(Ki(2) - 1)) + ...
              (zi(3).*(Ki(3) - 1))./(1 + vf.*(Ki(3) - 1)) + ...
              ((1-sum(zi))/vf);
    df = @(vf) (f(vf + 0.0001) - f(vf - 0.0001))/(2*0.0001);
    
    % Find a solution
    VF = 0.1; % initial guess...
    error = 1;
    
    while error > 0.001 % arbritrary tolerance
       error = VF; % error -> old VF
       
       
       VF = VF - (f(VF)/df(VF)); % Newton-Rhapson
       
        if VF < 0    % there's probably a slicker way to do this...
            VF = 0;
        elseif VF > 1
            VF = 1;
        end
       
       error = (VF - error)/error; % approximate error -> (new VF - old VF)/old VF
    end
       
end