%% Vapor Fraction Test Environment
clc, clear
% Script generates 2 figures:
%
%  T-VF diagram
%  T-x-y diagram for all condensible components
%% Test Variables
Tmin = 30; % BP Water
Tmax = 80; % BP HAc

stream = newStream('Name', 'Input',...   % Same condition from PFD
                       'P', 1,...        % that Fleischer graded
                       'MOLES', 860.3556943,...
                       'ET', 0.6177445976,...
                       'O2', 0.02581344803,...
                       'HAc',0.1446243601 ,...
                       'VAM',0.09247619553,...
                       'Water',0.09434866895,...
                       'CO2',0.01766397602,...
                       'C1',0.0007384685234,...
                       'C2',0.001723093221,...
                       'Ar',0.003893753611,...
                       'N2',0.0009734384027);

% stream = newStream('Name', 'Input',...
%                     'P', 1,...
%                     'MOLES', 1,...
%                     'Water', 0.3,...
%                     'HAc', 0.3,...
%                     'N2', 0.4);

%% Calculations

dewT(stream)
bubbleT(stream)

T = Tmin:1:Tmax;

% Allocate memory
VF = zeros(size(T));
xVAM = zeros(size(T));
yVAM = zeros(size(T));
xH2O = zeros(size(T));
yH2O = zeros(size(T));
xHAc = zeros(size(T));
yHAc = zeros(size(T));

% begin the loop...
for i = 1:length(T)

stream.T = T(i);
               
VF(i) = vaporFraction(stream);


K = [Psat('VAM',T(i))/stream.P,...
     Psat('Water',T(i))/stream.P,...
     Psat('HAc',T(i))/stream.P];


xVAM(i) = stream.VAM/(1 + VF(i)*(K(1) - 1));
yVAM(i) = xVAM(i)*K(1);

xH2O(i) = stream.Water/(1 + VF(i)*(K(2) - 1));
yH2O(i) = xH2O(i)*K(2);

xHAc(i) = stream.HAc/(1 + VF(i)*(K(3) - 1));
yHAc(i) = xHAc(i)*K(3);


end

% let's get the total non-condensible fraction for fun.
nonCond = 0;
comps = getCompList;
for i = 1:length(comps)
   if ~strcmp(comps{i},'VAM') && ...
            ~strcmp(comps{i},'Water') &&...
            ~strcmp(comps{i},'HAc')
        
        nonCond = nonCond + stream.(comps{i});
        
   end
end

subplot(2,2,1)
% Plot T-VF 
plot(VF,T,[nonCond nonCond],[T(1) T(end)])
ylabel('Temperature')
xlabel('Fraction')
title('VF-T Diagram')
legend('Vapor Fraction',...
        'Non Condensible Fraction')
axis([0,1,Tmin,Tmax])
grid
  
subplot(2,2,2)
% Plot T-x-y VAM
plot(xVAM,T,yVAM,T)
xlabel('Mole Fraction')
ylabel('Temperature')
title('VAM T-x-y Diagram')
legend('Liquid','Vapor')
axis([0,1,Tmin,Tmax])
grid

subplot(2,2,3)
% Plot T-x-y Water
plot(xH2O,T,yH2O,T)
xlabel('Mole Fraction')
ylabel('Temperature')
title('Water T-x-y Diagram')
legend('Liquid','Vapor')
axis([0,1,Tmin,Tmax])
grid

subplot(2,2,4)
% Plot T-x-y HAc
plot(xHAc,T,yHAc,T)
xlabel('Mole Fraction')
ylabel('Temperature')
title('HAc T-x-y Diagram')
legend('Liquid','Vapor')
axis([0,1,Tmin,Tmax])
 grid   