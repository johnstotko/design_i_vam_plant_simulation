function v = S(component,T,P)
% Gives the entropy for the specified component at temperature T and
% pressure P
%
% component     string, component from component list
% T             Temperature in C
% P             Pressure in bar

% Get the filename for the thermo data for that component
filename = ['data/', component, ' S.mat'];

% Load the table
data = struct2array(load(filename));

% Get the T,P and Values
T = data(2:end,1);
P = data(1,2:end)';

% Create te meshgrid
[T,P] = ndgrid(T,P);

V = data(2:end,2:end);

% Interpolate/Extrapolate
v = interp2(P,T,V,Pq,Tq,'spline');