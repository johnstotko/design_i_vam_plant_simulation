function vis = getViscosity(component, Tq, Pq)
% vis = getViscosity(component, Tq, Pq)
%
% Gives the molar heat capacity of component comp
vis = zeros(1,length(Tq));

for k = 1:length(Tq)
    switch component
        case 'HAc'
            
            if Tq(k) > 25
                vis(k) = 1.1393e3; %uPas http://www.ddbst.com/en/EED/PCP/VIS_C84.php
            else
                vis(k) = 9.16; % uPas http://www.ddbst.com/en/EED/PCP/VIS_C84.php
            end
            
        case 'VAM'
            
            
            if Tq(k) < 50
                vis(k) = 0.33e3; % uPas http://msdssearch.dow.com/PublishedLiteratureDOWCOM/dh_0980/0901b803809800b4.pdf?filepath=vam/pdfs/noreg/769-00002.pdf&fromPage=GetDoc
            else
                vis(k) = polyval([0.230604,84.33548],Tq); % https://app.knovel.com/web/view/itable/show.v/rcid:kpYHTPPCC4/cid:kt002UTA31/viewerType:eptble//root_slug:span-classmatchingviscosityspan-of-gas-live-eqns/url_slug:viscosity-of-gas?q=Vinyl%20Acetate%20viscosity&b-q=Vinyl%20Acetate%20viscosity&sort_on=default&b-subscription=true&b-group-by=true&b-sort-on=default&b-content-type=all_references&start=0&columns=6,1,2,3,4,5,7,8,9,10,11,13,14,15&sort=13&direction=DESC
            end
            
        otherwise

        filename = ['data/', component, ' Vis.mat'];

        % Load the table
        data = struct2array(load(filename));

        % Get the T,P and Values
        T = data(2:end,1);
        P = data(1,2:end)';

        % Create te meshgrid
        [T,P] = ndgrid(T,P);

        V = data(2:end,2:end);

        % Interpolate/Extrapolate
        vis(k) = interp2(P,T,V,Pq(k),Tq(k),'spline');      
    end
end