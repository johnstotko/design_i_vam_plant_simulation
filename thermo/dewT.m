function T = dewT(stream)
% T = dewT(stream)
%
% Gives the temperature (in C) above which the stream will be vapor.
%
% If there are no condensible components, dewT = -inf



x = [stream.VAM, stream.Water, stream.HAc];

if sum(x) == 0
    T = -inf;
    return
end

f = @(t)  x(1)*(stream.P/Psat('VAM',t)) + ...
          x(2)*(stream.P/Psat('Water',t)) + ...
          x(3)*(stream.P/Psat('HAc',t)) - 1;
         
T = fzero(@(t)f(t),60); % arbritraty starting point...

