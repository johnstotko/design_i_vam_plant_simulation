function hvap = getHvap(comp)
% hvap = getHvap(comp)
%
% Returns heat of vaporization for component comp
%
% kJ/mol

switch comp
    case 'Water'
        hvap = 43.988; %kJ/mol
    case 'VAM'
        hvap = 34.4; %kJ/mol
    case 'HAc'
        hvap = 23.7; %kJ/mol
    otherwise
   error(['No data on component ' comp])   
end