function a = activity(stream, comp)
% a = activity(stream, comp)
%
% Gives activity coefficients of component comp in stream according to the
% wilson model
%
% See table 1 in 
%
% An industrial design/control study for the vinyl acetate monomer process
%
% Michael L. Luyben* and Bjo?rn D. Tyre?us

ln_gamma = 1 - log(sumjXjAij(stream, comp))- sumk(stream, comp);

a = exp(ln_gamma);

end 


function s = sumk(stream, comp)

    s = 0;

    s = s + stream.VAM*wilsonParam('VAM',   comp,   stream.T)/...
                    sumjXjAij(stream, 'VAM');
    s = s + stream.HAc*wilsonParam('HAc',   comp,   stream.T)/...
                    sumjXjAij(stream, 'HAc');
    s = s + stream.Water*wilsonParam('Water', comp, stream.T)/...
                    sumjXjAij(stream, 'Water');
    
end

function s = sumjXjAij(stream, comp)

    s = 0;
    
    s = s + stream.VAM*wilsonParam(comp,   'VAM',   stream.T);
    s = s + stream.HAc*wilsonParam(comp,   'HAc',   stream.T);
    s = s + stream.Water*wilsonParam(comp, 'Water', stream.T);
    
end

function Aij = wilsonParam(comp1, comp2, T)
% Gives wilson parameter
%
T = T + 273.15; % Convert K -> C
R = 1.987; % cal/mol K , as per the paper

Aij = (getV(comp2)/getV(comp1))*exp((-wilsonA(comp1,comp2))/(R*T));


end

function a = wilsonA(comp1,comp2)

%    VAM        H20         HAc
data = [0          1384.6      -136.1;     % VAM
        2266.4      0           670.7 ;     % H20
        726.7       230.6       0     ];    % HAc

a = data(getNum(comp1),getNum(comp2));

end

function v = getV(comp)
    switch comp
        case 'VAM'
            v = 93.1; % ml/mol
        case 'Water' 
            v = 18.07; % ml/mol
        case 'HAc'
            v = 57.54; % ml/mol 
    end
end

function n = getNum(comp)
    switch comp
        case 'VAM'
            n = 1;
        case 'Water'
            n = 2;
        case 'HAc'
            n = 3;
    end
end