function T = bubbleT(stream)
% T = bubbleT(stream)
%
% Gives the temperature (In C) below which the stream will be liquid.
%
% Sum of all (xi Psat_i) = P


% If there are any non-condesnsible conponents, then the stream will always
% have a vapor fraction. Set T = -Inf


x = [stream.VAM stream.Water stream.HAc];

% if sum(x)< 0.99
%     T = -inf;
%     return
% end
p2 = 0;
t = 60;
while abs(stream.P - p2) > 0.01
    
    t = t + 0.005;
    
    p2 = x(1)*Psat('VAM',t) + x(2)*Psat('Water',t) + x(3)*Psat('HAc',t);
    
end
% f = @(t) x(1)*Psat('VAM',t) + ...
%          x(2)*Psat('Water',t) + ...
%          x(3)*Psat('HAc',t) - stream.P;
%          
% T = fzero(@(t)f(t),10); % arbritraty starting point...
end





