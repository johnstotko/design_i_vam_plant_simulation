function K = getK(stream, comp)
% function K = getK(stream, comp)
% 
% Gives equilibrium y/x for component comp in stream stream according to
% modified raoults law

K = Psat(comp,stream.T)/stream.P;