function h = getStreamH(stream)
% h = getStreamH(stream)
%
% Gives enthalpy of stream in kW

% make a struct. Keys = components, value = enthalpy
comps = getCompList;
enth = struct();

for i = 1:length(comps)
    enth.(comps{i}) = 0; 
end

% For all non-condensable components, use h function
nonCond = {'CO2',...
            'ET',...
            'N2',...
            'O2',...
            'C1',...
            'C2',...
            'Ar'};
        
for i = 1:length(nonCond)
    
    enth.(nonCond{i}) = H(nonCond{i},stream.T,stream.P)*...
                            stream.(nonCond{i})*...
                            stream.MOLES; % kJ
end

% Now for condensable components
cond = {'VAM',...
        'HAc',...
        'Water'};
    
Tref = 25; % Reference temperature in Celcius

% Bring components up to temp.
for i = 1:length(cond)
    enth.(cond{i}) = getCp(cond{i},Tref,1)*...
                            stream.(cond{i})*...
                            stream.MOLES*...
                            (stream.T - Tref)/1000; % kJ
end


% Add in the heat of vaporization

zi = zeros(1,length(cond));
for i = 1:length(cond)
    zi(i) = stream.(cond{i});
end

Ki = zeros(1,length(cond));
for i = 1:length(cond)
    Ki(i) = getK(stream, cond{i});
end

VF = vaporFraction(stream);
V = stream.MOLES*VF; % Moles of vapor

% Moles of vapor of each component
v = zeros(1,length(cond));
for i = 1:length(cond)
   v(i) = ((zi(i)*Ki(i))/(1 + VF*(Ki(i) - 1)))*V;
end

% Add heat of vaporization
for i = 1:length(cond)
   enth.(cond{i}) =  enth.(cond{i}) + v(i)*getHvap(cond{i});
end

% Add them all up
h = 0;
for i = 1:length(comps)
    h = h + enth.(comps{i});
end
    

            