function v = getLiquidVolume(stream)
% v = getRefVolume(stream)
%
% Gives volume of a liquid stream. Assumed not to change with T and P

comp = getCompList();

density = getStreamDensity(stream, 'liquid'); % kg/m^3

mass = 0;
% Get mass of each component
for i = 1:length(comp)
    mass = mass + stream.(comp{i})*stream.MOLES*molarMass(comp{i})/1000; %kg
end
v = mass/density;

