function C = getCv(component, Tq, Pq)
% C = getCv(component, Tq, Pq)
%
% Gives the molar heat capacity of component comp

for k = 1:length(Tq)
    switch component
        case 'HAc'
            C = 139.7; % J/molK
        case 'VAM'
            C = 169.5; % J/molK
        otherwise

        filename = ['data/', component, ' Cv.mat'];

        % Load the table
        data = struct2array(load(filename));

        % Get the T,P and Values
        T = data(2:end,1);
        P = data(1,2:end)';

        % Create te meshgrid
        [T,P] = ndgrid(T,P);

        V = data(2:end,2:end);

        % Interpolate/Extrapolate
        C(k) = interp2(P,T,V,Pq(k),Tq(k),'spline');      
    end
end