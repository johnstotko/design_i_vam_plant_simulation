function scv = getStreamCv(stream)
% scp = getStreamCp(stream)

comps = getCompList;
scv = 0;
for i = 1:length(comps)
    scv = scv + stream.(comps{i})*getCv(comps{i}, stream.T, stream.P);
end