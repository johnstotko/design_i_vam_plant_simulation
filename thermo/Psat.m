function c = Psat(comps,T)
% Gives the saturation pressure for the specified comps at temperature 
% T
%
% comps     string, comps from comps list
% T             Temperature in C
% P             Pressure in bar

if ~iscell(comps)
    comps = {comps};
end

c = zeros(1,length(comps));

for i = 1:length(comps)
    
    if getTc(comps{i}) > T
    
    filename = ['data/',comps{i},' Psat.mat'];
    PT = load(filename);
    PT = PT.Psat;
    
    c(i) = interp1(PT(:,1),PT(:,2),T,'linear','extrap');
    
    % Catch if there is no Psat Data
    if isnan(c(i))
        c(i) = Inf;
    end
        
    clear PT
    
    else
        
        c(i) = Inf;
        
    end
      
end