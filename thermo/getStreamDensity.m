function p = getStreamDensity(stream, phase)
%  p = getDensity(stream)
%
% Gives density in kg per cubic meter. 

switch phase
    
    case 'liquid'
        % Only VAM, HAc, and Water
        
        waterDensity = 1000; %[kg/m^3]
        vamDensity = 0.93*waterDensity;  %[kg/m^3]
        hacDensity = 1.05*waterDensity;  %[kg/m^3]
        
        massWater = moles2mass('Water',stream.Water*stream.MOLES);
        massVAM = moles2mass('VAM',stream.VAM*stream.MOLES);
        massHAC = moles2mass('HAc',stream.HAc*stream.MOLES);
        
        totMass = massWater + massVAM + massHAC;
        
        p = (waterDensity*massWater + vamDensity*massVAM + hacDensity*massHAC)/totMass;
        
    case 'vapor'
        % Ideal gas
        R = 8.3144598*10^-5; % [(bar m^3)/(mol K)]
        % Get average molar mass.
        comps = getCompList;
        molarMassAvg = 0;
        
        for i = 1:length(comps)
            molarMassAvg = molarMassAvg + stream.(comps{i})*molarMass(comps{i}); %[g/mol]
        end
        
        % p = (n*MW)/V = (P*MW)/(RT)
        
        p = (stream.P * molarMassAvg)/(R * (stream.T + 273.15))/1000; % [kg/m^3]
    otherwise
        p = NaN;
        
end