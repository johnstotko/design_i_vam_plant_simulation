function scp = getStreamCp(stream)
% scp = getStreamCp(stream)

comps = getCompList;
scp = 0;
for i = 1:length(comps)
    scp = scp + stream.(comps{i})*getCp(comps{i},stream.T,stream.P);
end