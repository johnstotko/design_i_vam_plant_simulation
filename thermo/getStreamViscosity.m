function vis = getStreamViscosity(stream)
% vis = getStreamViscosity(stream)
%
% Gets the weighted average ciscisity for the stream. Assumes ideal
% solution.

comps = getCompList;

vis = 0;

for i = 1:length(comps)
   
    vis = vis + stream.(comps{i})*getViscosity(comps{i},stream.T,stream.P);
    
end

