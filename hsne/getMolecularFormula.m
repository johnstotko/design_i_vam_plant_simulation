function molec = getMolecularFormula(comp)
% molec = getMolecularFormula(comp)
%
% Gives molecular formula as an array [C, H, O]
switch comp
    case 'ET'
        molec(1) = 2;
        molec(2) = 4;
        molec(3) = 0;
    case 'HAc'
        molec(1) = 2;
        molec(2) = 4;
        molec(3) = 2;
    case 'VAM'
        molec(1) = 2;
        molec(2) = 6;
        molec(3) = 2;
    case 'C1'
        molec(1) = 1;
        molec(2) = 4;
        molec(3) = 0;
    case 'C2'
        molec(1) = 2;
        molec(2) = 6;
        molec(3) = 0;
    otherwise 
        molec(1) = 0;
        molec(2) = 0;
        molec(3) = 0;
end
        