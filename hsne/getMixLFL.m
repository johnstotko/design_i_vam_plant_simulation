function lfl = getMixLFL(stream)
% lfl = getMixLFL(stream)
%
% Gives lower flamability limit of stream
%
% Gives inf if stream contains no carbon.

comps = getFuelList;
fuel = getFuel(stream);

if fuel.MOLES ~= 0
    denom = 0;
    for i = 1:length(comps)
        denom = denom + fuel.(comps{i})/(getCompLFL(comps{i},stream.T));
    end

    lfl = 1/denom;
else
    lfl = nan;
end

end

function lfl = getCompLFL(comp,T)
% ufl = getCompUFL(comp, T)
%
% Gives component upper flammability limit at specified Temperature and
% Pressure.
%
% Gives ufl = -inf if component does not contian carbon.

lfl = getCompLFLatSTP(comp) + ((0.75)/getHeatofComb(comp))*(T - 25);

end

function lfl = getCompLFLatSTP(comp)
% lfl = getCompLFL(comp)
%
% Gives component lower flammability limit at 25C according to molecular
% correlations
%
% Gives lfl = inf if component does not contian carbon.
molecularFormula = getMolecularFormula(comp);

if molecularFormula(1) ~= 0
lfl = 55/(4.76*molecularFormula(1) + ...
            1.19*molecularFormula(2) + ...
            2.38*molecularFormula(3) + 1);
else
lfl = inf;
end

end