function loc = getLOC(stream)
% loc = getLOC(stream)
%
% Get's the limiting oxygen content for a fuel mixture in stream.
%
% LOC = z*LFL
%
% where z is 
%
% Fuel + z O2 -> Combustion Products

fuel = getFuel(stream);

loc = getAverageO2StoichCoeff(fuel)*getMixLFL(stream);
end

