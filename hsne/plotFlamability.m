function fig = plotFlamability(stream, stoichiometricLine, locLine)

A_title = 'Fuel';
A_abv = 'Pure Fuel';

B_title = 'Inert Species';
B_abv = 'Inert';

C_title = 'Oxygen';
C_abv = 'O2';

% NOTE when plotting the ternPlot, we're plotting (Oxygen, Inert)
air = [0.21 ,   0.79;
            0   ,   0   ];
        
[airX, airY] = rect2tern(air(:,1),air(:,2));

fig = figure;
hold on
ternPlot(A_title,A_abv,B_title,B_abv,C_title,C_abv)
plot(airX, airY,'k')

% Limiting Oxygen Content (LOC) line
loc = getLOC(stream)/100;
[locX, locY] = rect2tern([loc; loc],[1-loc; 0]);

if locLine
plot(locX, locY,'k')
end

% Stoichiometric Coeff Line
z = getAverageO2StoichCoeff(getFuel(stream));
[stcX, stcY] = rect2tern([z/(z+1); 0], [0; 1]);

if stoichiometricLine
plot(stcX, stcY,'k')
end

% LOC and STC intersection
kneeX = loc;
kneeY = -loc*((1+z)/z) + 1;
[kneeXtern, kneeYtern] = rect2tern(kneeX,kneeY);
plot(kneeXtern, kneeYtern, 'k.')

% UFL and AIR Intersection
A = [0.21   -0.79;
     1          1];
b = [0;
     1 - getMixUFL(stream)/100];
x = A\b;
uflX = x(2);
uflY = x(1);

[uflXtern, uflYtern] = rect2tern(uflX, uflY);
plot(uflXtern, uflYtern, 'k.')


% LFL and AIR Intersection
A = [0.21   -0.79;
     1          1];
b = [0;
     1 - getMixLFL(stream)/100];
x = A\b;
lflX = x(2);
lflY = x(1);

[lflXtern, lflYtern] = rect2tern(lflX, lflY);
plot(lflXtern, lflYtern, 'k.')

% Upper Flammability Line
M = (kneeY - uflY)/(kneeX - uflX);

A = [0          1;
     M         -1];
b = [0;
     M*kneeX - kneeY];
x = A\b;

UOLX = x(1);
UOLY = x(2);

[UOLXtern, UOLYtern] = rect2tern(UOLX, UOLY);
plot(UOLXtern, UOLYtern, 'k.')
plot([UOLXtern,kneeXtern], [UOLYtern, kneeYtern], 'k--')

% Lower Flamability Line
M = (kneeY - lflY)/(kneeX - lflX);

A = [0          1;
     M         -1];
b = [0;
     M*kneeX - kneeY];
x = A\b;

LOLX = x(1);
LOLY = x(2);

[LOLXtern, LOLYtern] = rect2tern(LOLX, LOLY);
plot(LOLXtern, LOLYtern, 'k.')
plot([LOLXtern,kneeXtern], [LOLYtern, kneeYtern], 'k--')

% Put where the stream is
[sO2tern, sInertTern] = rect2tern(stream.O2, 1 - stream.O2 - getFuelFrac(stream));
plot(sO2tern, sInertTern, 'ko')