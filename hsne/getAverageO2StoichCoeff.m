function z = getAverageO2StoichCoeff(fuel)
% z = getAverageO2StoichCoeff(fuel)
%
% Get's the weighted average z for the fuel mixture

z = 0;
comps = getFuelList;
for i = 1:length(comps)
    z = z + fuel.(comps{i})*getO2StoichCoeff(comps{i});
end

end

function z = getO2StoichCoeff(comp)
% z = getO2StoichCoeff(comp)
%
% Get's the stoichiometric coefficient of O2 in
%
% comp + O2 -> CO2 + H2O

CmHxOy = getMolecularFormula(comp);

z = CmHxOy(1) + CmHxOy(2)/4  - CmHxOy(3)/2;

end