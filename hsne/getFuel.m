function fuel = getFuel(stream)
% fuel = getFuel(stream)
%
% Gives a fuel object (similar to stream object) that just contains the
% fuel components of stream.

fuel = struct('MOLES', 0);

comps = getFuelList;

for i = 1:length(comps)
    fuel.MOLES = fuel.MOLES + stream.(comps{i})*stream.MOLES;
    fuel.(comps{i}) = stream.(comps{i})*stream.MOLES;
end

for i = 1:length(comps)
    fuel.(comps{i}) = fuel.(comps{i})/fuel.MOLES;
end