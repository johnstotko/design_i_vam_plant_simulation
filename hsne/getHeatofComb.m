function h = getHeatofComb(comp)
% h = getHeatofComb(comp)
%
% Gives heat of combustion for component in [kcal/mol]
%
% Gives 1 if comp does not contian carbon.

switch comp
    case 'ET'
        h = -337.23; % kcal/mol
    case 'HAc'
        h = -209.02; % kcal/mol
    case 'VAM'
        h = -5.419*molarMass('VAM'); % kcal/mol
    case 'C1'
        h = -212.79; % kcal/mol
    case 'C2'
        h = -372.81; % kcal/mol
    otherwise
        h = 1;
end