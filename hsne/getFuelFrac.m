function f = getFuelFrac(stream)
% f = getFuelFrac(stream)
%
% Get mole fraction of stream that is fuel

fuel = getFuel(stream);
f = fuel.MOLES/stream.MOLES;