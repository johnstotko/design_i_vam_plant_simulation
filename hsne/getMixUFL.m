function ufl = getMixUFL(stream)
% lfl = getMixUFL(stream)
%
% Gives upper flamability limit of stream
%
% Gives -inf if stream contains no carbon.

comps = getFuelList;
fuel = getFuel(stream);


if fuel.MOLES ~= 0
    denom = 0;
    for i = 1:length(comps)
        denom = denom + fuel.(comps{i})/(getCompUFL(comps{i}, stream.T));
    end

    ufl = 1/denom;
else
    ufl = nan;
end

end

function ufl = getCompUFL(comp,T)
% ufl = getCompUFL(comp, T)
%
% Gives component upper flammability limit at specified Temperature and
% Pressure.
%
% Gives ufl = -inf if component does not contian carbon.

ufl = getCompUFLatSTP(comp) + ((0.75)/getHeatofComb(comp))*(T - 25);

end

function ufl = getCompUFLatSTP(comp)
% ufl = getCompUFL(comp)
%
% Gives component upper flammability limit at 25C and 1 bar according to 
% molecular correlations
%
% Gives ufl = -inf if component does not contian carbon.
molecularFormula = getMolecularFormula(comp);

if molecularFormula(1) ~= 0
ufl = 350/(4.76*molecularFormula(1) + ...
            1.19*molecularFormula(2) + ...
            2.38*molecularFormula(3) + 1);
else 
ufl = -inf;
end


end