function unit = newMixer(varargin)

unit = struct(  'Name', [],...
                'Inputs', [],... Stream names
                'Outputs', [],... Stream name
                'Pressure_Drop', 0);
            
for i = 1:2:nargin
    unit.(varargin{i}) = varargin{i+1};   
end