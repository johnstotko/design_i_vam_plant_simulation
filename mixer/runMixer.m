function [unit, streams] = runMixer(unit, streams)
% [unit, streams] = runMixer(unit, streams)
%
% This unit model mixes streams
%
% streams       Array of process stream structs
% unit          Mixer unit struct

% Get input stream indexes
Inputs = [];
for i = 1:length([unit.Inputs])
Inputs = [Inputs, findStream(streams, unit.Inputs{i})];
end

% Get output stream indexes
Output = findStream(streams, unit.Outputs);

% Add the materials
streams(Output).MOLES = sum([streams(Inputs).MOLES]);


% CHECK IF OUTPUT IS ZERO
if streams(Output).MOLES ~= 0

comps = getCompList();
for i = 1:length(comps)
    streams(Output).(comps{i}) = ...
        sum([streams(Inputs).(comps{i})].*[streams(Inputs).MOLES])/streams(Output).MOLES;
end


% Pressure
streams(Output).P = max([streams(Inputs).P]);

% Temperature
cp = zeros(1,length(Inputs));

for i = 1:length(comps)
    cp = cp + ([streams(Inputs).(comps{i})] .* getCp(comps{i},[streams(Inputs).T],[streams(Inputs).P]));
end

streams(Output).T = sum([streams(Inputs).MOLES].*cp.*[streams(Inputs).T]) ./ sum([streams(Inputs).MOLES].*cp);


end