function [unit, streams] = runValve(unit, streams)
% [unit, streams] = runValve(unit, streams)

% Get stream indexes
Input = findStream(streams, unit.Input);
Output = findStream(streams, unit.Output);

if streams(Input).MOLES ~= 0
T1 = streams(Input).T + 273.15;
P1 = streams(Input).P;
P2 = unit.TargetPressure;

comps = getCompList;

Cp = getStreamCp(streams(Input));

Cv = getStreamCv(streams(Input));

gamma = Cp/Cv;

% Set T and P
streams(Output).P = unit.TargetPressure;
streams(Output).T = (T1 * ((P2/P1)^((gamma-1)/gamma))) - 273.15;

% Composition/Mole Flow is identical
streams(Output).MOLES = streams(Input).MOLES;

for i = 1:length(comps)
   streams(Output).(comps{i}) = streams(Input).(comps{i}); 
end

end
