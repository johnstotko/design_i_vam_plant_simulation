function [unit, streams] = runAbsorber(unit, streams)
% unit      an instance of the absorber struct. Type newAbsorber in the
%               Command Window to see what fields an absorber struct has. 
%
% streams   an array of streams. Each stream(n) is a instance of the stream
%               struct. Type newStream in the Command Window to see what
%               fields a stream struct has.
%
% See also: newAbsorber, newStream, getCompList.


L_IN = findStream(streams, unit.L_IN);
V_IN = findStream(streams, unit.V_IN);
L_OUT = findStream(streams, unit.L_OUT);
V_OUT = findStream(streams, unit.V_OUT);

if streams(V_IN).MOLES ~= 0 && streams(L_IN).MOLES ~= 0
% Absorber



% Temperature
Inputs = [L_IN, V_IN];

cp = zeros(1,length(Inputs));

comps = getCompList();
for i = 1:length(comps)
    cp = cp + ([streams(Inputs).(comps{i})] .* getCp(comps{i},[streams(Inputs).T],[streams(Inputs).P]));
end

T = sum([streams(Inputs).MOLES].*cp.*[streams(Inputs).T]) ./ sum([streams(Inputs).MOLES].*cp);

V = streams(V_IN).MOLES; % Total vapor , change to more general variables
L = 0; % Iterated for later
N = unit.NumberOfStages; % Either have to specify L or N, since this is considering pure water Absorbent
phiv = unit.PhiVAM;
RHS = 1;


Kv = Psat('VAM',T)/streams(V_IN).P;
Ka = Psat('HAc',T)/streams(V_IN).P;
Kw = Psat('Water',T)/streams(V_IN).P;

while abs(phiv - RHS) > 0.001
    
    L = L + 0.1;
    Aev = L / (Kv * V);
    RHS = (Aev - 1) / ((Aev^(N+1)) - 1);
    
end

% The L here actually specifies the utility water + the raffinate
%
% Need to make sure we run the absorber, run mixer 2, then run the absorber
% again so everything makes sense.
%
% Get the stream name for the make up water

CWS = findStream(streams, 'CWS');
RAF = findStream(streams, 'RAFFINATE');

% MKW is all spec'ed out except for the mole flow, so...
streams(CWS).MOLES = L - streams(RAF).MOLES;

% And on with the rest of the absorber..
Aea = L / (Ka * V);
Aew = L / (Kw * V);

phia = (Aea - 1) / ((Aea^(N+1)) - 1);
phiw = (Aew - 1) / ((Aew^(N+1)) - 1);

streams(L_OUT).VAM = streams(V_IN).VAM * ...
                     streams(V_IN).MOLES *...
                     (1-phiv) + ...
                     streams(L_IN).VAM * ...
                     streams(L_IN).MOLES;
                 
streams(L_OUT).Water = streams(V_IN).Water * ...
                       streams(V_IN).MOLES * ...
                       (1-phiw) + ...
                       streams(L_IN).Water * ...
                       streams(L_IN).MOLES;
                   
streams(L_OUT).HAc = streams(V_IN).HAc * ...
                     streams(V_IN).MOLES * ...
                     (1-phia) + ...
                     streams(L_IN).HAc * ...
                     streams(L_IN).MOLES;

streams(V_OUT).VAM = streams(V_IN).VAM * ...
                     streams(V_IN).MOLES * ...
                     (phiv);
                 
streams(V_OUT).Water = streams(V_IN).Water * ...
                       streams(V_IN).MOLES * ...
                       (phiw);
                   
streams(V_OUT).HAc = streams(V_IN).HAc * ...
                     streams(V_IN).MOLES * ...
                     (phia);

% All other components go to vapor out
for i = 1:length(comps)
    if ~strcmp(comps{i},'VAM') && ...
       ~strcmp(comps{i},'Water') && ...
       ~strcmp(comps{i},'HAc')
   
    streams(V_OUT).(comps{i}) = streams(V_IN).(comps{i}) * ...
                                streams(V_IN).MOLES;
    
    end
end


% Convert back to mole fraction
streams(V_OUT).MOLES = 0;
streams(L_OUT).MOLES = 0;

for i = 1:length(comps)
    streams(V_OUT).MOLES = streams(V_OUT).MOLES + ...
                            streams(V_OUT).(comps{i});
    streams(L_OUT).MOLES = streams(L_OUT).MOLES + ...
                            streams(L_OUT).(comps{i});
end


for i = 1:length(comps)
   streams(V_OUT).(comps{i}) = streams(V_OUT).(comps{i})/...
                                streams(V_OUT).MOLES;
                            
   streams(L_OUT).(comps{i}) = streams(L_OUT).(comps{i})/...
                                streams(L_OUT).MOLES;
end


% Temperature and pressure.
streams(V_OUT).T = T;
streams(L_OUT).T = T;

streams(V_OUT).P = streams(L_IN).P;
streams(L_OUT).P = streams(V_IN).P;


end



