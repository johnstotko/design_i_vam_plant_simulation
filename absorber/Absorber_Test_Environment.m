%% Absorber Test Evnironment
%
% Use this script to test out your absorber functions. This is seperate
% from the proccess simulation.

%% DEFINE THE STREAMS
% We only need 4 streams, 2 of them are specified.
streams(1) = newStream('Name', 'RAFFINATE',...
                      'T', 50, ...
                      'P', 5, ...
                      'MOLES', 100...
                      'Water', 0.995,...
                      'HAc', 0.004,...
                      'VAM', 0.001);
                  
streams(2) = newStream('Name', 'RXN VAP',...
                      'T', 50, ...
                      'P', 5,...
                      'MOLES', ,...
                      'ET', ,...
                      'O2', ,...
                      'HAc', ,...
                      'VAM', ,...
                      'Water', ,...
                      'CO2', ,...
                      'C1', ,...
                      'C2', ,...
                      'Ar', ,...
                      'N2', );
                  
streams(3) = newStream('Name', 'CLEAN_VAP');
streams(4) = newStream('Name', 'DIRTY WATER');

%% CREATE THE UNIT
% Only one unit for this test, the absorber.

absorbers(1) = newAbsorber('Name', 'Absorber',...
                          'NumberOfStages', 1,...
                          'L_IN', 'RAFFINATE',...
                          'V_IN', 'RXN_VAP', ...
                          'L_OUT', 'DIRTY WATER',...
                          'V_OUT', 'CLEAN VAP');
                      
%% RUN THE SIMULATION

[absorbers(1), streams] = runAbsorber(absorbers(1), streams);

%% POST TEST
%
% Simple tests, easily overlooked stuff that can be the cause of a bug

isMoleFracSum1(streams);
isMolesNonNegative(streams);

%% DISPLAY STUFF

disp('STREAMS')
disp(struct2table(streams))
disp(' ')
disp(' ')
disp('UNITS')
disp(struct2table(absorbers))
disp(' ')
disp(' ')
                        