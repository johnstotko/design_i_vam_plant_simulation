function unit = newAbsorber(varargin)

unit = struct( 'Name', [], ... % Unit Name
               'NumberOfStages', 1,... % Stage count
               'L_IN', [], ... % Liquid input stream name
               'V_IN', [], ... % Vapor input stream name
               'L_OUT', [], ... % Liquid output stream name
               'V_OUT', [],...; % Vapor Output stream name
               'PhiVAM', 0);
               
for i = 1:2:nargin
    unit.(varargin{i}) = varargin{i+1};   
end